<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:url value="/student" var="students" />
<c:url value="/professor" var="professors" />
<c:url value="/subject" var="subjects" />
<c:url value="/exam" var="exams" />
<c:url value="/application" var="applications" />

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav mx-auto">
				<li class="nav-item "><a class="nav-link"
					href="<c:out value="${students}"/>">Work with students</a></li>
				<li class="nav-item "><a class="nav-link"
					href="<c:out value="${professors}"/>">Work with professors</a></li>
				<li class="nav-item "><a class="nav-link"
					href="<c:out value="${subjects}"/>">Work with subjects</a></li>
				<li class="nav-item "><a class="nav-link"
					href="<c:out value="${exams}"/>">Work with exams</a></li>
				<li class="nav-item "><a class="nav-link"
					href="<c:out value="${applications}"/>">Work with exam
						applications</a></li>
			</ul>
		</div>
	</div>
</nav>