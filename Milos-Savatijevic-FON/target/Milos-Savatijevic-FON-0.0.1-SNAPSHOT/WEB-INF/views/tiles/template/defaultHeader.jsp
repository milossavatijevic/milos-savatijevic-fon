<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<security:authorize access="isAuthenticated()"> Authenticated as:
	<security:authentication property="principal.username" />
</security:authorize>

<c:url value="/authentication/logout" var="logout"></c:url>
<c:url value="/home" var="logo"></c:url>

<div>
	<header class="py-4 bg-primary">
	<div>
			<a class="text-white text-decoration-none" style="position: absolute; top: 8px; right: 16px; font-size: 18px;" href="<c:out value="${logout}"/>">Logout</a>
			</div><div>
			<a class="p-5 text-left text-white display-4 text-decoration-none"
				href="<c:out value="${logo}"/>">Logo</a>
				</div>
	</header>
</div>