<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>index</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<style>
	#container{
		height: 70vh;
	}
	#title{
		padding: 50px;
	}
</style>
</head>
<body>
	<div class="container fill" id="container">
		<div class="row">
			<div class="mx-auto">
					<h1 id="title">There is a problem with login page!</h1>
			</div>
		</div>
	</div>
</body>
</html>