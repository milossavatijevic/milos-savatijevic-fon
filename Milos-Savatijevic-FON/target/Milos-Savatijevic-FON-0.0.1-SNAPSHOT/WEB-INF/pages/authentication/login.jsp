<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<style>
.error {
	color: red;
}
</style>

<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center text-primary">Sign In</h5>
						<c:set value="${pageContext.request.contextPath}"
							var="contextPath"></c:set>
						<form:form class="form-signin"
							action="${contextPath}/authentication/login" method="post"
							modelAttribute="userDto">
							<c:if test="${not empty errorMessage}">
								<div class="error">${errorMessage}</div>
							</c:if>
							<div class="form-label-group">
								<form:label class="col-lg-5 col-form-label text-primary"
									path="username">Username:</form:label>
								<form:input class="form-control" type="text" path="username"
									id="username" />
								<form:errors path="username" cssClass="error" />
							</div>
							<p />
							<div class="form-label-group">
								<form:label class="col-lg-5 col-form-label text-primary"
									path="password">Password:</form:label>
								<form:input class="form-control" type="password" path="password"
									id="password" />
								<form:errors path="password" cssClass="error" />
							</div>
							<p />
							<br />
							<button id="Login"
								class="btn btn-lg btn-primary btn-block text-uppercase">Login</button>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>