<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit subject</title>
<style>
	#container {
	height: 70vh;
}
</style>
</head>
<body>
	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-7 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Edit subject</h2>
						<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>
						<form:form method="post"
							action="${contextPath}/subject/editSave"
							modelAttribute="subjectDto" class="form-horizontal">
							<fieldset>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="name">Name:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="name" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="description">Description:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="description" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label"
										path="yearOfStudy">Year of study:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="yearOfStudy" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="semester">Semester</form:label>
									<div class="col-sm-8">
										<form:select class="form-control" path="semester" disabled="true">
											<form:options items="${semesters}" itemValue="semester"
												itemLabel="semester" />
										</form:select>
									</div>
								</div>

								<h3>Are you sure you want to edit subject?</h3>
								<div class="form-group">
									<div class="col-md-4">
										<button class="btn btn-primary" id="edit" name="action"
											value="edit">Edit</button>
										<button class="btn btn-primary" id="cancel" name="action"
											value="cancel">Cancel</button>
									</div>
								</div>
							</fieldset>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>