<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<style>
	#container{
		height: 70vh;
	}
	#title{
		padding: 50px;
	}
</style>
</head>
<body>
	<div class="container fill" id="container">
		<div class="row">
			<div class="mx-auto">
					<h1 id="title">Milos_Savatijevic_FON</h1>
			</div>
		</div>
	</div>
</body>
</html>


