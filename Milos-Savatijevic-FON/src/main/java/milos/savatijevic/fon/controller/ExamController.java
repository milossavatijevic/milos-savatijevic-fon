package milos.savatijevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.exception.MyValidationException;
import milos.savatijevic.fon.service.ExamService;
import milos.savatijevic.fon.service.ProfessorService;
import milos.savatijevic.fon.service.SubjectService;

@Controller
@RequestMapping(value = "/exam")
@SessionAttributes(value = { "examDto" })
public class ExamController {

	private final ProfessorService professorService;
	private final SubjectService subjectService;
	private final ExamService examService;

	@Autowired
	public ExamController(ProfessorService professorService, SubjectService subjectService, ExamService examService) {
		this.professorService = professorService;
		this.subjectService = subjectService;
		this.examService = examService;
	}

	
	@GetMapping
	public ModelAndView home(@RequestParam(name = "page", required = false) Integer page,  SessionStatus session) {
		System.out.println("====================================================================");
		System.out.println("====================   ExamController: home()    ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/exams");
		List<ExamDto> exams = examService.getAll();
		PagedListHolder<ExamDto> pagedListHolder = new PagedListHolder<>(exams);
		pagedListHolder.setPageSize(3);
		modelAndView.addObject("maxPages", pagedListHolder.getPageCount());
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			page = 1;
			modelAndView.addObject("page", page);
			pagedListHolder.setPage(0);
			modelAndView.addObject("exams", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelAndView.addObject("exams", pagedListHolder.getPageList());
		}
		session.setComplete();
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("============= ExamController: add()   ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/add");
		return modelAndView;
	}

	@ModelAttribute(name = "examDto")
	private ExamDto getExamDto() {
		System.out.println("====================================================================");
		System.out.println("======ExamController: getExamDto() ============");
		System.out.println("====================================================================");
		ExamDto examDto = new ExamDto();
		return examDto;
	}

	@ModelAttribute(name = "professors")
	private List<ProfessorDto> getAllProfessors() {
		System.out.println("====================================================================");
		System.out.println("============= ExamController: getAllProfessors() ============");
		System.out.println("====================================================================");
		return professorService.getAll();
	}

	@ModelAttribute(name = "subjects")
	private List<SubjectDto> getAllSubjects() {
		System.out.println("====================================================================");
		System.out.println("============= ExamController: getAllSubjects() ============");
		System.out.println("====================================================================");
		return subjectService.getAll();
	}

	@ModelAttribute(name = "exams")
	private List<ExamDto> getAllExams() {
		System.out.println("====================================================================");
		System.out.println("============= ExamController: getAllExams() ============");
		System.out.println("====================================================================");
		return examService.getAll();
	}

	@PostMapping(value = "save")
	public String save(@Valid @ModelAttribute(name = "examDto") ExamDto examDto, Errors errors)
			throws MyValidationException {
		
		if (errors.hasErrors())
			return "exam/add";
		
		examService.save(examDto);
		return "redirect:/exam/add";
	}

}
