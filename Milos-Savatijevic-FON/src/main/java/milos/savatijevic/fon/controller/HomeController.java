package milos.savatijevic.fon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/home")
public class HomeController {

	@GetMapping
	public ModelAndView home() {
		System.out.println("====================================================================");
		System.out.println("====================   HomeController: home()    ===================");
		System.out.println("====================================================================");
		return new ModelAndView("home");
	}
}
