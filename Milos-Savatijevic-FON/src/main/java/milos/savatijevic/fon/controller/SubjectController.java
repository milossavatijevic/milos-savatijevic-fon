package milos.savatijevic.fon.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.enums.Semester;
import milos.savatijevic.fon.service.SubjectService;
import milos.savatijevic.fon.validator.SubjectDtoValidator;

@Controller
@RequestMapping(value = "/subject")
@SessionAttributes(value = { "subjectDto" })
public class SubjectController {

	private final SubjectService subjectService;

	@Autowired
	SubjectController(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	
	@GetMapping
	public ModelAndView home(@RequestParam(name = "page", required = false) Integer page, 
			/* @RequestParam(name = "num", required = false) String num, */ SessionStatus session) {
		System.out.println("====================================================================");
		System.out.println("====================   SubjectController: home()    ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("subject/subjects");
//		if(num==null) {
//			num="1";
//		}
		List<SubjectDto> subjects = subjectService.getAll();
		PagedListHolder<SubjectDto> pagedListHolder = new PagedListHolder<>(subjects);
//		pagedListHolder.setPageSize(Integer.parseInt(num));
//		modelAndView.addObject("num", num);
		pagedListHolder.setPageSize(3);
		modelAndView.addObject("maxPages", pagedListHolder.getPageCount());
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			page = 1;
			modelAndView.addObject("page", page);
			pagedListHolder.setPage(0);
			modelAndView.addObject("subjects", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelAndView.addObject("subjects", pagedListHolder.getPageList());
		}
		session.setComplete();
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("============= SubjectController: add()   ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("subject/add");
		return modelAndView;
	}

	@ModelAttribute(name = "subjectDto")
	private SubjectDto getSubjectDto() {
		System.out.println("====================================================================");
		System.out.println("======SubjectController: getSubjectDto() ============");
		System.out.println("====================================================================");
		SubjectDto subjectDto = new SubjectDto();
		subjectDto.setName("-");
		subjectDto.setYearOfStudy(0L);
		return subjectDto;
	}

	@ModelAttribute(name = "semesters")
	private List<Semester> getSemesters() {
		System.out.println("====================================================================");
		System.out.println("============= SubjectController: getSemesters() ============");
		System.out.println("====================================================================");
		return new ArrayList<Semester>(Arrays.asList(Semester.values()));
	}

	@ModelAttribute(name = "subjects")
	private List<SubjectDto> getAllSubjects() {
		System.out.println("====================================================================");
		System.out.println("============= SubjectController: getAllSubjects() ============");
		System.out.println("====================================================================");
		return subjectService.getAll();
	}

	@PostMapping(value = "addSave")
	public String addSave(@ModelAttribute(name = "subjectDto") SubjectDto subjectDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= SubjectController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("save")) {
			subjectService.save(subjectDto);
			sessionStatus.setComplete();
		}
		return "redirect:/subject/add";
	}

	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("subject/edit").addObject("subjectDto",
				subjectService.findById(id));
		return modelAndView;
	}

	@GetMapping(value = "details")
	public ModelAndView details(@RequestParam(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("subject/details").addObject("subjectDto",
				subjectService.findById(id));
		return modelAndView;
	}

	@GetMapping(value = "delete")
	public String delete(@RequestParam(name = "id") Long id) {
		SubjectDto subjectDto = subjectService.findById(id);
		subjectService.delete(subjectDto);

		return "redirect:/subject";
	}

	@PostMapping(value = "addConfirm")
	public ModelAndView addConfirm(@Valid @ModelAttribute(name = "subjectDto") SubjectDto subjectDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("subject/add");
		} else {
			modelAndView.setViewName("subject/addConfirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "editSave")
	public ModelAndView editSave(@ModelAttribute(name = "subjectDto") SubjectDto subjectDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= SubjectController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("edit")) {
			subjectService.update(subjectDto);
		}

		return new ModelAndView("subject/edit").addObject("subjectDto", subjectDto);
	}

	@PostMapping(value = "editConfirm")
	public ModelAndView editConfirm(@Valid @ModelAttribute(name = "subjectDto") SubjectDto subjectDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {

			modelAndView.setViewName("subject/edit");
		} else {
			modelAndView.setViewName("subject/editConfirm");
		}

		return modelAndView;
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public String sqlExceptionHandler(DataIntegrityViolationException dataIntegrityViolationException,RedirectAttributes redirectAttributes) {
		
		redirectAttributes.addFlashAttribute("errorMessage", "cannot be deleted because of created exams");
		
		return "redirect:/subject";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new SubjectDtoValidator());
	}
	

	

}
