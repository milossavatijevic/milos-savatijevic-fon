package milos.savatijevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.service.CityService;
import milos.savatijevic.fon.service.StudentService;
import milos.savatijevic.fon.validator.StudentDtoValidator;

@Controller
@RequestMapping(value = "/student")
@SessionAttributes(value = { "studentDto"})
public class StudentController {
	private final CityService cityService;
	private final StudentService studentService;

	@Autowired
	StudentController(StudentService studentService, CityService cityService) {
		this.studentService = studentService;
		this.cityService = cityService;
	}

	
	@GetMapping
	public ModelAndView home(@RequestParam(name = "page", required = false) Integer page,  SessionStatus session) {
		System.out.println("====================================================================");
		System.out.println("====================   StudentController: home()    ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("student/students");
		List<StudentDto> students = studentService.getAll();
		PagedListHolder<StudentDto> pagedListHolder = new PagedListHolder<>(students);
		pagedListHolder.setPageSize(3);
		modelAndView.addObject("maxPages", pagedListHolder.getPageCount());
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			page = 1;
			modelAndView.addObject("page", page);
			pagedListHolder.setPage(0);
			modelAndView.addObject("students", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelAndView.addObject("students", pagedListHolder.getPageList());
		}
		session.setComplete();
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("============= StudentController: add()   ===================");
		System.out.println("====================================================================");

		ModelAndView modelAndView = new ModelAndView("student/add");
		return modelAndView;
	}

	@ModelAttribute(name = "studentDto")
	private StudentDto getStudentDto() {
		System.out.println("====================================================================");
		System.out.println("======StudentController: getStudentDto() ============");
		System.out.println("====================================================================");
		StudentDto studentDto = new StudentDto();
		studentDto.setFirstname("-");
		studentDto.setLastname("-");
		studentDto.setIndexNumber("-");
		studentDto.setEmail("-");
		studentDto.setAddress("-");
		studentDto.setPhone("0");
		studentDto.setCurrentYearOfStudy(0L);
		return studentDto;
	}

	@ModelAttribute(name = "cities")
	private List<CityDto> getAllCities() {
		System.out.println("====================================================================");
		System.out.println("============= StudentController: getAllCities() ============");
		System.out.println("====================================================================");
		return cityService.getAll();
	}

	@ModelAttribute(name = "students")
	private List<StudentDto> getAllStudents() {
		System.out.println("====================================================================");
		System.out.println("============= StudentController: getAllStudents() ============");
		System.out.println("====================================================================");
		return studentService.getAll();
	}

	@PostMapping(value = "addConfirm")
	public ModelAndView addConfirm(@Valid @ModelAttribute(name = "studentDto") StudentDto studentDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("student/add");
		} else {
			modelAndView.setViewName("student/addConfirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "editConfirm")
	public ModelAndView editConfirm(@Valid @ModelAttribute(name = "studentDto") StudentDto studentDto,
			Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			
				modelAndView.setViewName("student/edit");
		} else {
			modelAndView.setViewName("student/editConfirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "addSave")
	public String addSave(@ModelAttribute(name = "studentDto") StudentDto studentDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= StudentController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("save")) {
			studentService.save(studentDto);
			sessionStatus.setComplete();
		}

		return "redirect:/student/add";
	}

	@PostMapping(value = "editSave")
	public ModelAndView editSave(@ModelAttribute(name = "studentDto") StudentDto studentDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= StudentController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("edit")) {
			studentService.update(studentDto);
		}
		return new ModelAndView("student/edit").addObject("studentDto", studentDto);
	}

	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") Long id) {
	ModelAndView modelAndView = new ModelAndView("student/edit").addObject("studentDto", studentService.findById(id));
	return modelAndView;
	}
	
	@GetMapping(value = "details")
	public ModelAndView details(@RequestParam(name = "id") Long id) {
	ModelAndView modelAndView = new ModelAndView("student/details").addObject("studentDto", studentService.findById(id));
	return modelAndView;
	}

	@GetMapping(value = "delete")
	public String delete(@RequestParam(name = "id") Long id) {
		StudentDto studentDto = studentService.findById(id);
		studentService.delete(studentDto);
		
		return "redirect:/student";
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public String sqlExceptionHandler(DataIntegrityViolationException dataIntegrityViolationException,RedirectAttributes redirectAttributes) {
		
		redirectAttributes.addFlashAttribute("errorMessage", "cannot be deleted because of registered exams");
		
		return "redirect:/student";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new StudentDtoValidator(studentService));
	}
	
}
