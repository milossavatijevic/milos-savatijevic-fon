package milos.savatijevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.dto.TitleDto;
import milos.savatijevic.fon.service.CityService;
import milos.savatijevic.fon.service.ProfessorService;
import milos.savatijevic.fon.service.TitleService;
import milos.savatijevic.fon.validator.ProfessorDtoValidator;

@Controller
@RequestMapping(value = "/professor")
@SessionAttributes(value = { "professorDto"})
public class ProfessorController {
	private final CityService cityService;
	private final ProfessorService professorService;
	private final TitleService titleService;

	@Autowired
	ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService) {
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}
	
	@GetMapping
	public ModelAndView home(@RequestParam(name = "page", required = false) Integer page,  SessionStatus session) {
		System.out.println("====================================================================");
		System.out.println("====================   ProfessorController: home()    ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/professors");
		List<ProfessorDto> professors = professorService.getAll();
		PagedListHolder<ProfessorDto> pagedListHolder = new PagedListHolder<>(professors);
		pagedListHolder.setPageSize(3);
		modelAndView.addObject("maxPages", pagedListHolder.getPageCount());
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			page = 1;
			modelAndView.addObject("page", page);
			pagedListHolder.setPage(0);
			modelAndView.addObject("professors", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelAndView.addObject("professors", pagedListHolder.getPageList());
		}
		session.setComplete();
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: add()   ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/add");
		return modelAndView;
	}
	
	@ModelAttribute(name = "professorDto")
	private ProfessorDto getProfessorDto() {
		System.out.println("====================================================================");
		System.out.println("======ProfessorController: getProfessorDto() ============");
		System.out.println("====================================================================");
		ProfessorDto professorDto = new ProfessorDto();
		professorDto.setFirstname("-");
		professorDto.setLastname("-");
		professorDto.setEmail("-");
		professorDto.setAddress("-");
		professorDto.setPhone("-");
		return professorDto;
	}

	@ModelAttribute(name = "cities")
	private List<CityDto> getAllCities() {
		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: getAllCities() ============");
		System.out.println("====================================================================");
		return cityService.getAll();
	}
	
	@ModelAttribute(name = "titles")
	private List<TitleDto> getAllTitles() {
		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: getAllTitles() ============");
		System.out.println("====================================================================");
		return titleService.getAll();
	}
	
	@ModelAttribute(name = "professors")
	private List<ProfessorDto> getAllProfessors() {
		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: getAllProfessors() ============");
		System.out.println("====================================================================");
		return professorService.getAll();
	}
	
	@PostMapping(value = "addSave")
	public String addSave(@ModelAttribute(name = "professorDto") ProfessorDto professorDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("save")) {
			professorService.save(professorDto);
			sessionStatus.setComplete();
		}
		return "redirect:/professor/add";
	}
	
	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") Long id) {
	ModelAndView modelAndView = new ModelAndView("professor/edit").addObject("professorDto", professorService.findById(id));
	return modelAndView;
	}
	
	@GetMapping(value = "details")
	public ModelAndView details(@RequestParam(name = "id") Long id) {
	ModelAndView modelAndView = new ModelAndView("professor/details").addObject("professorDto", professorService.findById(id));
	return modelAndView;
	}

	@GetMapping(value = "delete")
	public String delete(@RequestParam(name = "id") Long id) throws DataIntegrityViolationException {
		ProfessorDto professorDto = professorService.findById(id);
		professorService.delete(professorDto);
		
		return "redirect:/professor";
	}
	
	@PostMapping(value = "addConfirm")
	public ModelAndView addConfirm(@Valid @ModelAttribute(name = "professorDto") ProfessorDto professorDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("professor/add");
		} else {
			modelAndView.setViewName("professor/addConfirm");
		}

		return modelAndView;
	}
	
	@PostMapping(value = "editSave")
	public ModelAndView editSave(@ModelAttribute(name = "professorDto") ProfessorDto professorDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus) {

		System.out.println("====================================================================");
		System.out.println("============= ProfessorController: save() =================");
		System.out.println("====================================================================");

		if (action.equalsIgnoreCase("edit")) {
			professorService.update(professorDto);
		}

		return new ModelAndView("professor/edit").addObject("professorDto", professorDto);
	}
	
	@PostMapping(value = "editConfirm")
	public ModelAndView editConfirm(@Valid @ModelAttribute(name = "professorDto") ProfessorDto professorDto,
			Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			
				modelAndView.setViewName("professor/edit");
		} else {
			modelAndView.setViewName("professor/editConfirm");
		}

		return modelAndView;
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public String sqlExceptionHandler(DataIntegrityViolationException dataIntegrityViolationException,RedirectAttributes redirectAttributes) {
		
		redirectAttributes.addFlashAttribute("errorMessage", "cannot be deleted because of exams");
		
		return "redirect:/professor";
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfessorDtoValidator(professorService));
	}
}
