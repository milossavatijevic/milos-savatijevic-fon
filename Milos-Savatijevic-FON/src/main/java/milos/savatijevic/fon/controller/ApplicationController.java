package milos.savatijevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import milos.savatijevic.fon.dto.ApplicationDto;
import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.exception.ExamRegisterException;
import milos.savatijevic.fon.service.ApplicationService;
import milos.savatijevic.fon.service.ExamService;
import milos.savatijevic.fon.service.StudentService;

@Controller
@RequestMapping(value = "/application")
@SessionAttributes(value = { "applicationDto"})
public class ApplicationController {
	
	private final ExamService examService;
	private final StudentService studentService;
	private final ApplicationService applicationService;
	@Autowired
	public ApplicationController(ExamService examService, StudentService studentService,
			ApplicationService applicationService) {
		super();
		this.examService = examService;
		this.studentService = studentService;
		this.applicationService = applicationService;
	}

	
	@GetMapping
	public ModelAndView home(@RequestParam(name = "page", required = false) Integer page,  SessionStatus session) {
		System.out.println("====================================================================");
		System.out.println("====================   ApplicationController: home()    ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("application/applications");
		List<ApplicationDto> applications = applicationService.getAll();
		PagedListHolder<ApplicationDto> pagedListHolder = new PagedListHolder<>(applications);
		pagedListHolder.setPageSize(3);
		modelAndView.addObject("maxPages", pagedListHolder.getPageCount());
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			page = 1;
			modelAndView.addObject("page", page);
			pagedListHolder.setPage(0);
			modelAndView.addObject("applications", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelAndView.addObject("applications", pagedListHolder.getPageList());
		}
		session.setComplete();
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("============= ApplicationController: add()   ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("application/add");
		return modelAndView;
	}
	
	@ModelAttribute(name = "applicationDto")
	private ApplicationDto getApplicationDto() {
		System.out.println("====================================================================");
		System.out.println("======ApplicationController: getApplicationDto() ============");
		System.out.println("====================================================================");
		ApplicationDto applicationDto = new ApplicationDto();
		return applicationDto;
	}
	
	@ModelAttribute(name = "students")
	private List<StudentDto> getAllStudents() {
		System.out.println("====================================================================");
		System.out.println("============= ApplicationController: getAllStudents() ============");
		System.out.println("====================================================================");
		return studentService.getAll();
	}
	
	@ModelAttribute(name = "exams")
	private List<ExamDto> getNewExams() {
		System.out.println("====================================================================");
		System.out.println("============= ApplicationController: getNewExams() ============");
		System.out.println("====================================================================");
		return examService.getNewExams();
	}
	
	@ModelAttribute(name = "applications")
	private List<ApplicationDto> getAllApplications() {
		System.out.println("====================================================================");
		System.out.println("============= ApplicationController: getAllApplications() ============");
		System.out.println("====================================================================");
		return applicationService.getAll();
	}
	
	@PostMapping(value = "save")
	public String save(@Valid @ModelAttribute(name = "applicationDto") ApplicationDto applicationDto) throws ExamRegisterException {
		applicationService.save(applicationDto);
		return "redirect:/application/add";
	}
}
