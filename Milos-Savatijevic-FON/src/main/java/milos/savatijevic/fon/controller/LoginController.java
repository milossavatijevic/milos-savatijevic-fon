package milos.savatijevic.fon.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import milos.savatijevic.fon.dto.UserDto;
import milos.savatijevic.fon.security.config.DatabaseAuthenticationProvider;

@Controller
@RequestMapping(value = "/authentication")
public class LoginController {
	@Autowired
	private DatabaseAuthenticationProvider databaseAuthenticationProvider;

	@GetMapping(value = "login")
	public ModelAndView login() {
		System.out.println("AuthenticationController: get()=======================");
		return new ModelAndView("authentication/login");
	}

	@PostMapping(value = "login")
	public ModelAndView authenticate(@ModelAttribute("userDto") UserDto userDto) {
		System.out.println("===============================================================");
		System.out.println("AuthenticationController: authenticate()=======================");
		System.out.println("===============================================================");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");

//		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDto.getUsername(),
//				userDto.getPassword());
//		Authentication authentication = databaseAuthenticationProvider.authenticate(token);
//
//		if (authentication == null) {
//			modelAndView.setViewName("authentication/login");
//		} else {
//			SecurityContextHolder.getContext().setAuthentication(authentication);
//		}
		return modelAndView;
	}
	
	@ModelAttribute(name = "userDto")
	private UserDto generateUserDto() {
		return new UserDto();
	}

	@GetMapping(value = "logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (auth != null) {
//			new SecurityContextLogoutHandler().logout(request, response, auth);
//		}
		return "redirect:/authentication/login";
	}

}
