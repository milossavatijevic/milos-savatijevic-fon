package milos.savatijevic.fon.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import milos.savatijevic.fon.validator.PhoneValidator;

@Constraint(validatedBy = PhoneValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PhoneNumber {
	String message();
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
