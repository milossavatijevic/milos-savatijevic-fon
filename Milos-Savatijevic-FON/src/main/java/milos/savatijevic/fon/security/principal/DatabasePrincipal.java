package milos.savatijevic.fon.security.principal;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import milos.savatijevic.fon.dto.UserDto;


public class DatabasePrincipal implements UserDetails, Serializable{

	private static final long serialVersionUID = 3119956931533090903L;
	
	private final UserDto userDto;
	
	public DatabasePrincipal(UserDto userDto) {
		this.userDto = userDto;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return userDto.getPassword();
	}

	@Override
	public String getUsername() {
		return userDto.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}
	
	public UserDto getUserDto() {
		return userDto;
	}

}
