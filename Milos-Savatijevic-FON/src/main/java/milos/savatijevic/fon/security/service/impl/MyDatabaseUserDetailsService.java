package milos.savatijevic.fon.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import milos.savatijevic.fon.converter.UserConverter;
import milos.savatijevic.fon.dto.UserDto;
import milos.savatijevic.fon.entity.UserEntity;
import milos.savatijevic.fon.repository.jpa.UserJpaRepository;
import milos.savatijevic.fon.security.principal.DatabasePrincipal;
import milos.savatijevic.fon.security.service.MyUserDetailsService;
@Service
public class MyDatabaseUserDetailsService implements MyUserDetailsService{
	
	@Autowired
	UserJpaRepository userJpaRepository;
	@Autowired 
	UserConverter userConverter;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userJpaRepository.findByUsername(username);
		if (user!=null) {
			UserDto userDto = userConverter.entityToDto(user);
			return new DatabasePrincipal(userDto);
		}
		throw new UsernameNotFoundException("Invalid username or password!");
	}

}
