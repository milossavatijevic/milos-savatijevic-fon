package milos.savatijevic.fon.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import milos.savatijevic.fon.security.principal.DatabasePrincipal;
import milos.savatijevic.fon.security.service.MyUserDetailsService;

@Component
public class DatabaseAuthenticationProvider implements AuthenticationProvider{
	
	@Autowired
	MyUserDetailsService userDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getPrincipal().toString();
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
		
		if (userDetails instanceof DatabasePrincipal) {
			
			if (token.getCredentials().toString().equals(userDetails.getPassword())) {
				System.out.println("=========================================================");
				System.out.println("===================SUCCESS LOGIN  =======================");
				System.out.println("=========================================================");
				
				DatabasePrincipal databasePrincipal = (DatabasePrincipal)userDetails;
				UsernamePasswordAuthenticationToken auth = 
						new UsernamePasswordAuthenticationToken(
								databasePrincipal.getUserDto(),
								databasePrincipal.getAuthorities());
				
				return auth;

			} else {
				System.out.println("=========================================================");
				System.out.println("===================NOT SUCCESS LOGIN ====================");
				System.out.println("=========================================================");
			}
		}

		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
