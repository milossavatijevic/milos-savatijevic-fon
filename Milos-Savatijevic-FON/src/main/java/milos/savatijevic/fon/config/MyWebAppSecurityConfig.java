package milos.savatijevic.fon.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import milos.savatijevic.fon.security.config.DatabaseAuthenticationProvider;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "milos.savatijevic.fon.security.config", "milos.savatijevic.fon.security.service" })
public class MyWebAppSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DatabaseAuthenticationProvider databaseAuthenticationProvider;

	MyWebAppSecurityConfig() {
		System.out.println("=================================================================");
		System.out.println("================= MyWebAppSecurityConfig =============");
		System.out.println("=================================================================");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.authenticationProvider(databaseAuthenticationProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("=================================================================");
		System.out.println("=============MyWebAppSecurityConfig: configure=======================");
		System.out.println("=================================================================");

//		http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/authentication/login")
//				.permitAll();
		
	}

}
