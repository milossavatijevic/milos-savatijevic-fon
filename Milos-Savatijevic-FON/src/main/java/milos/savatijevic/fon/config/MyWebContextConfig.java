package milos.savatijevic.fon.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import milos.savatijevic.fon.formatter.CityDtoFormatter;
import milos.savatijevic.fon.formatter.ExamDtoFormatter;
import milos.savatijevic.fon.formatter.ProfessorDtoFormatter;
import milos.savatijevic.fon.formatter.SemesterFormatter;
import milos.savatijevic.fon.formatter.StudentDtoFormatter;
import milos.savatijevic.fon.formatter.SubjectDtoFormatter;
import milos.savatijevic.fon.formatter.TitleDtoFormatter;
import milos.savatijevic.fon.service.CityService;
import milos.savatijevic.fon.service.ExamService;
import milos.savatijevic.fon.service.ProfessorService;
import milos.savatijevic.fon.service.StudentService;
import milos.savatijevic.fon.service.SubjectService;
import milos.savatijevic.fon.service.TitleService;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { 
		"milos.savatijevic.fon.controller",
		"milos.savatijevic.fon.exception"
})
public class MyWebContextConfig implements WebMvcConfigurer {
	private CityService cityService;
	private TitleService titleService;
	private ProfessorService professorService;
	private SubjectService subjectService;
	private StudentService studentService;
	private ExamService examService;

	@Autowired
	public MyWebContextConfig(CityService cityService, TitleService titleService, ProfessorService professorService, SubjectService subjectService,
			StudentService studentService, ExamService examService) {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
		this.cityService = cityService;
		this.titleService = titleService;
		this.professorService = professorService;
		this.subjectService = subjectService;
		this.studentService = studentService;
		this.examService = examService;
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	@Bean
	public ViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setOrder(0);
		return tilesViewResolver;
	}
	
	@Bean
	public TilesConfigurer tilesCongigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(
				new String[] {"/WEB-INF/views/tiles/tiles.xml"}
		);
		return tilesConfigurer;
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:/authentication/login");
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new SemesterFormatter());
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
		registry.addFormatter(new ExamDtoFormatter(examService));
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
}
