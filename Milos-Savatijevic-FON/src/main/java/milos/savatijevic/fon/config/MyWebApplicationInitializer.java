package milos.savatijevic.fon.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import milos.savatijevic.fon.config.MyWebContextConfig;

public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	public MyWebApplicationInitializer() {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebApplicationInitializer =========================");
		System.out.println("=================================================================");
	}
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {MyDatabaseConfig.class, MyWebAppSecurityConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {MyWebContextConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}

}
