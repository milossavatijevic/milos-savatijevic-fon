package milos.savatijevic.fon.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
@ComponentScan(basePackages = { 
		"milos.savatijevic.fon.service", 
		"milos.savatijevic.fon.converter",
		"milos.savatijevic.fon.repository" })
@PropertySources({
	@PropertySource("classpath:database_config.properties")
})
@EnableJpaRepositories(
		basePackages = "milos.savatijevic.fon.repository.jpa"
)
@EnableTransactionManagement
public class MyDatabaseConfig {
	
	@Autowired
	private Environment env;

	@Bean
	public DataSource datasource() {
		DriverManagerDataSource datasource = new DriverManagerDataSource();
//		datasource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//		datasource.setUrl("jdbc:mysql://localhost:3306/milos_savatijevic_fon?useLegacyDatetimeCode=false&serverTimezone=UTC");
//		datasource.setUsername("root");
//		datasource.setPassword("Norge2012");
		datasource.setDriverClassName(env.getProperty("database.driverClassName"));
		datasource.setUrl(env.getProperty("database.url"));
		datasource.setUsername(env.getProperty("database.username"));
		datasource.setPassword(env.getProperty("database.password"));
		return datasource;
	}
	
//	@Bean
//    public SpringLiquibase liquibase()  {
//        SpringLiquibase liquibase = new SpringLiquibase();
//        liquibase.setDataSource(datasource());
//        liquibase.setChangeLog("classpath:db_changelog-master.xml");
//        return liquibase;
//    }

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(datasource());

		em.setPackagesToScan(new String[] {"milos.savatijevic.fon.entity"});

		HibernateJpaVendorAdapter  jpaVendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(jpaVendorAdapter);
		em.setJpaProperties(getAdditionProperties());
		return em;
	}

	private Properties getAdditionProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.show_sql", "true");
//		properties.setProperty("hibernate.hbm2ddl.auto", "create");
//		properties.setProperty("hibernate.format_sql", "true");
//		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		return properties;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
}
