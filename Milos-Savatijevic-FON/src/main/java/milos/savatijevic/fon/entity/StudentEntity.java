package milos.savatijevic.fon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import milos.savatijevic.fon.entity.CityEntity;

@Entity
@Table(name = "student")
@NamedQuery(name = "findAllStudents", query = "SELECT s FROM StudentEntity s")
public class StudentEntity implements Serializable{

	private static final long serialVersionUID = -68662879260479956L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STUDENT_ID")
	private Long id;
	@Column(name = "INDEX_NUMBER", length = 10, unique = true, nullable = false)
	private String indexNumber;
	@Column(name = "FIRSTNAME", length = 30, nullable = false)
	private String firstname;
	@Column(name = "LASTNAME", length = 30, nullable = false)
	private String lastname;
	@Column(name = "EMAIL", length = 30, unique = true)
	private String email;
	@Column(name = "ADDRESS", length = 50)
	private String address;
	@Column(name = "PHONE", length = 15)
	private String phone;
	@Column(name = "CURRENT_YEAR_OF_STUDY", length = 7, nullable = false)
	private Long currentYearOfStudy;
	@ManyToOne
	@JoinColumn(name = "CITY_ID")
	private CityEntity cityEntity;
	
	public StudentEntity(Long id, String indexNumber, String firstname, String lastname, String email, String address,
			String phone, Long currentYearOfStudy, CityEntity cityEntity) {
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
		this.cityEntity = cityEntity;
	}

	public StudentEntity() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public CityEntity getCityEntity() {
		return cityEntity;
	}

	public void setCityEntity(CityEntity cityEntity) {
		this.cityEntity = cityEntity;
	}

	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", email=" + email + ", address=" + address + ", phone=" + phone + ", currentYearOfStudy="
				+ currentYearOfStudy + ", cityEntity=" + cityEntity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentEntity other = (StudentEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		return true;
	}
	
	

}
