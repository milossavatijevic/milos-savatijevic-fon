package milos.savatijevic.fon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "city")
public class CityEntity implements Serializable {

	private static final long serialVersionUID = 7355037065039120668L;
	@Id
	@Column(name = "PTT_NUMBER", length = 10)
	private Long pttNumber;
	@Column(name = "NAME", length = 30)
	private String name;
	
	public CityEntity() {
		
	}

	public CityEntity(Long pttNumber, String name) {
		this.pttNumber = pttNumber;
		this.name = name;
	}

	public Long getPttNumber() {
		return pttNumber;
	}

	public void setPttNumber(Long pttNumber) {
		this.pttNumber = pttNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityEntity [pttNumber=" + pttNumber + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pttNumber == null) ? 0 : pttNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CityEntity other = (CityEntity) obj;
		if (pttNumber == null) {
			if (other.pttNumber != null)
				return false;
		} else if (!pttNumber.equals(other.pttNumber))
			return false;
		return true;
	}
	
	

}

