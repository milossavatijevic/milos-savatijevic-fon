package milos.savatijevic.fon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name = "professor")
@NamedQuery(name = "findAllProfessors", query = "SELECT p FROM ProfessorEntity p")
public class ProfessorEntity implements Serializable{

	private static final long serialVersionUID = -3733897124826395531L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PROFESSOR_ID")
	private Long id;
	@Column(name = "FIRSTNAME", length = 30, nullable = false)
	private String firstname;
	@Column(name = "LASTNAME", length = 30, nullable = false)
	private String lastname;
	@Column(name = "EMAIL", length = 30, unique = true)
	private String email;
	@Column(name = "ADDRESS", length = 50)
	private String address;
	@Column(name = "PHONE", length = 15)
	private String phone;
	@Column(name = "REELECTION_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date reelectionDate;
	@ManyToOne
	@JoinColumn(name = "CITY_ID")
	private CityEntity cityEntity;
	@ManyToOne
	@JoinColumn(name = "TITLE_ID")
	private TitleEntity titleEntity;

	public ProfessorEntity() {
		
	}

	public ProfessorEntity(Long id, String firstname, String lastname, String email, String address, String phone,
			Date reelectionDate, CityEntity cityEntity, TitleEntity titleEntity) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.cityEntity = cityEntity;
		this.titleEntity = titleEntity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public CityEntity getCityEntity() {
		return cityEntity;
	}

	public void setCityEntity(CityEntity cityEntity) {
		this.cityEntity = cityEntity;
	}

	public TitleEntity getTitleEntity() {
		return titleEntity;
	}

	public void setTitleEntity(TitleEntity titleEntity) {
		this.titleEntity = titleEntity;
	}

	@Override
	public String toString() {
		return "ProfessorEntity [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", address=" + address + ", phone=" + phone + ", reelectionDate=" + reelectionDate + ", cityEntity="
				+ cityEntity + ", titleEntity=" + titleEntity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorEntity other = (ProfessorEntity) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
