package milos.savatijevic.fon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "exam")
@NamedQueries({
	@NamedQuery(name = "findAllExams", query = "SELECT e FROM ExamEntity e ORDER BY e.examDate DESC"),
	@NamedQuery(name = "findExamByDateAndSubject", 
	query = "SELECT e FROM ExamEntity e WHERE e.examDate>=current_date AND e.subjectEntity=:subject"),
	@NamedQuery(name = "findNewExams", 
	query = "SELECT e FROM ExamEntity e WHERE e.examDate>=current_date")
})
public class ExamEntity implements Serializable {

	private static final long serialVersionUID = -5402168188170668822L;
	@Id
	@Column(name = "EXAM_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "EXAM_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date examDate;
	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID")
	private SubjectEntity subjectEntity;
	@ManyToOne
	@JoinColumn(name = "PROFESSOR_ID")
	private ProfessorEntity professorEntity;
	
	
	public ExamEntity() {
		
	}

	public ExamEntity(Long id, Date examDate, SubjectEntity subjectEntity, ProfessorEntity professorEntity) {
		super();
		this.id = id;
		this.examDate = examDate;
		this.subjectEntity = subjectEntity;
		this.professorEntity = professorEntity;
	}


	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public SubjectEntity getSubjectEntity() {
		return subjectEntity;
	}

	public void setSubjectEntity(SubjectEntity subjectEntity) {
		this.subjectEntity = subjectEntity;
	}

	public ProfessorEntity getProfessorEntity() {
		return professorEntity;
	}

	public void setProfessorEntity(ProfessorEntity professorEntity) {
		this.professorEntity = professorEntity;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ExamEntity [id=" + id + ", examDate=" + examDate + ", subjectEntity=" + subjectEntity
				+ ", professorEntity=" + professorEntity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examDate == null) ? 0 : examDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((professorEntity == null) ? 0 : professorEntity.hashCode());
		result = prime * result + ((subjectEntity == null) ? 0 : subjectEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamEntity other = (ExamEntity) obj;
		if (examDate == null) {
			if (other.examDate != null)
				return false;
		} else if (!examDate.equals(other.examDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (professorEntity == null) {
			if (other.professorEntity != null)
				return false;
		} else if (!professorEntity.equals(other.professorEntity))
			return false;
		if (subjectEntity == null) {
			if (other.subjectEntity != null)
				return false;
		} else if (!subjectEntity.equals(other.subjectEntity))
			return false;
		return true;
	}

}

