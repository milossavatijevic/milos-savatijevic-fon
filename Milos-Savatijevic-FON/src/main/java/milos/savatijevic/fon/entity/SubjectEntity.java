package milos.savatijevic.fon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import milos.savatijevic.fon.enums.Semester;
@Entity
@Table(name = "subject")
@NamedQuery(name = "findAllSubjects", query = "SELECT s FROM SubjectEntity s")
public class SubjectEntity implements Serializable{

	private static final long serialVersionUID = 2457288336926402203L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SUBJECT_ID")
	private Long id;
	@Column(name = "NAME", length = 30, nullable = false)
	private String name;
	@Column(name = "DESCRIPTION", length = 200)
	private String description;
	@Column(name = "YEAR_OF_STUDY", length = 1)
	private Long yearOfStudy;
	@Enumerated(EnumType.STRING)
	@Column(name = "SEMESTER")
	private Semester semester;

	public SubjectEntity() {
		
	}

	public SubjectEntity(Long id, String name, String description, Long yearOfStudy, Semester semester) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectEntity other = (SubjectEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}
