package milos.savatijevic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.ApplicationDto;
import milos.savatijevic.fon.entity.ApplicationEntity;

@Component
public class ApplicationConverter {

	private final StudentConverter studentConverter;
	private final ExamConverter examConverter;
	@Autowired
	public ApplicationConverter(StudentConverter studentConverter, ExamConverter examConverter) {
		this.studentConverter = studentConverter;
		this.examConverter = examConverter;
	}
	
	public ApplicationDto entityToDto(ApplicationEntity applicationEntity) {
		return new ApplicationDto(applicationEntity.getId(), examConverter.entityToDto(applicationEntity.getExamEntity()),
				studentConverter.entityToDto(applicationEntity.getStudentEntity()));
	}
	
	public ApplicationEntity dtoToEntity(ApplicationDto applicationDto) {
		return new ApplicationEntity(applicationDto.getId(), examConverter.dtoToEntity(applicationDto.getExamDto()),
				studentConverter.dtoToEntity(applicationDto.getStudentDto()));
	}
	
}
