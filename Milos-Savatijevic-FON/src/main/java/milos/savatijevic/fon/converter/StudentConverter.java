package milos.savatijevic.fon.converter;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.entity.CityEntity;
import milos.savatijevic.fon.entity.StudentEntity;

@Component
public class StudentConverter {
	
	public StudentDto entityToDto(StudentEntity studentEntity) {
		return new StudentDto(studentEntity.getId(), studentEntity.getIndexNumber(), studentEntity.getFirstname(),
				studentEntity.getLastname(), studentEntity.getEmail(), studentEntity.getAddress(), studentEntity.getPhone(),
				studentEntity.getCurrentYearOfStudy(),
				new CityDto(studentEntity.getCityEntity().getPttNumber(), studentEntity.getCityEntity().getName()));
	}
	
	public StudentEntity dtoToEntity(StudentDto studentDto) {
		return new StudentEntity(studentDto.getId(), studentDto.getIndexNumber(),
				studentDto.getFirstname(), studentDto.getLastname(), studentDto.getEmail(), studentDto.getAddress(),
				studentDto.getPhone(), studentDto.getCurrentYearOfStudy(),
				new CityEntity(studentDto.getCityDto().getPttNumber(), studentDto.getCityDto().getName()));
	}
}
