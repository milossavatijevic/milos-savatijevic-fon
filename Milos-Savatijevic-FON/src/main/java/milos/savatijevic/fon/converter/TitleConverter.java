package milos.savatijevic.fon.converter;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.TitleDto;
import milos.savatijevic.fon.entity.TitleEntity;

@Component
public class TitleConverter {
	
	public TitleDto entityToDto(TitleEntity titleEntity) {
		return new TitleDto(titleEntity.getId(), titleEntity.getName());
	}
}
