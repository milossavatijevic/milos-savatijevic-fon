package milos.savatijevic.fon.converter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.entity.ExamEntity;
import milos.savatijevic.fon.entity.SubjectEntity;

@Component
public class ExamConverter {
	
	private final ProfessorConverter professorConverter;
	@Autowired
	public ExamConverter(ProfessorConverter professorConverter) {
		this.professorConverter = professorConverter;
	}

	public ExamDto entityToDto(ExamEntity examEntity) {
		return new ExamDto(examEntity.getId(), dateToString(examEntity), new SubjectDto(examEntity.getSubjectEntity().getId(), examEntity.getSubjectEntity().getName(),
				examEntity.getSubjectEntity().getDescription(), examEntity.getSubjectEntity().getYearOfStudy(), examEntity.getSubjectEntity().getSemester()),
				professorConverter.entityToDto(examEntity.getProfessorEntity()));
				
				
	}
	
	public ExamEntity dtoToEntity(ExamDto examDto) {
		return new ExamEntity(examDto.getId(), stringToDate(examDto), new SubjectEntity(examDto.getSubjectDto().getId(), examDto.getSubjectDto().getName(),
				examDto.getSubjectDto().getDescription(), examDto.getSubjectDto().getYearOfStudy(), examDto.getSubjectDto().getSemester()),
				professorConverter.dtoToEntity(examDto.getProfessorDto()));
				
				
	}
	
	private String dateToString(Object object) {
		ExamEntity examEntity = (ExamEntity)object;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = new Date(examEntity.getExamDate().getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		String examDate = formatter.format(date);
		return examDate;
	}
	
	private Date stringToDate(Object object) {
		ExamDto examDto = (ExamDto)object;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(examDto.getExamDate(), formatter);
		Date examDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return examDate;
	}
}
