package milos.savatijevic.fon.converter;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.entity.CityEntity;

@Component
public class CityConverter {
	
	public CityDto entityToDto(CityEntity cityEntity) {
		return new CityDto(cityEntity.getPttNumber(), cityEntity.getName());
	}
	
}
