package milos.savatijevic.fon.converter;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.UserDto;
import milos.savatijevic.fon.entity.UserEntity;

@Component
public class UserConverter {

	public UserDto entityToDto(UserEntity userEntity) {
		return new UserDto(userEntity.getId(), userEntity.getUsername(), userEntity.getPassword(),
				userEntity.getFirstname(), userEntity.getLastname());
	}

	public UserEntity dtoToEntity(UserDto userDto) {
		return new UserEntity(userDto.getId(), userDto.getFirstname(), userDto.getLastname(), userDto.getUsername(),
				userDto.getPassword());
	}

}
