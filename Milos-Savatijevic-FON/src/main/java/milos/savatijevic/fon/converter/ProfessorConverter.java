package milos.savatijevic.fon.converter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.dto.TitleDto;
import milos.savatijevic.fon.entity.CityEntity;
import milos.savatijevic.fon.entity.ProfessorEntity;
import milos.savatijevic.fon.entity.TitleEntity;

@Component
public class ProfessorConverter {

	public ProfessorDto entityToDto(ProfessorEntity professorEntity) {
		return new ProfessorDto(professorEntity.getId(), professorEntity.getFirstname(), professorEntity.getLastname(),
				professorEntity.getEmail(), professorEntity.getAddress(), professorEntity.getPhone(), dateToString(professorEntity),
				new CityDto(professorEntity.getCityEntity().getPttNumber(), professorEntity.getCityEntity().getName()),
				new TitleDto(professorEntity.getTitleEntity().getId(), professorEntity.getTitleEntity().getName()));
	}
	
	public ProfessorEntity dtoToEntity(ProfessorDto professorDto) {
		return new ProfessorEntity(professorDto.getId(), professorDto.getFirstname(), professorDto.getLastname(),
				professorDto.getEmail(), professorDto.getAddress(), professorDto.getPhone(), stringToDate(professorDto),
				new CityEntity(professorDto.getCityDto().getPttNumber(), professorDto.getCityDto().getName()),
				new TitleEntity(professorDto.getTitleDto().getId(), professorDto.getTitleDto().getName()));
	}
	
	private String dateToString(Object object) {
		ProfessorEntity professorEntity = (ProfessorEntity)object;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = new Date(professorEntity.getReelectionDate().getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		String reelectionDate = formatter.format(date);
		return reelectionDate;
	}
	
	private Date stringToDate(Object object) {
		ProfessorDto professorDto = (ProfessorDto)object;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(professorDto.getReelectionDate(), formatter);
		Date reelectionDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return reelectionDate;
	}
}
