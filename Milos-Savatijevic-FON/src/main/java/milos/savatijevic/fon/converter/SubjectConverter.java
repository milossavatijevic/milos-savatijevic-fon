package milos.savatijevic.fon.converter;

import org.springframework.stereotype.Component;

import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.entity.SubjectEntity;

@Component
public class SubjectConverter {

	public SubjectDto entityToDto(SubjectEntity subjectEntity) {
		return new SubjectDto(subjectEntity.getId(), subjectEntity.getName(), subjectEntity.getDescription(), 
				subjectEntity.getYearOfStudy(), subjectEntity.getSemester());
	}
	
	public SubjectEntity dtoToEntity(SubjectDto subjectDto) {
		return new SubjectEntity(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(), 
				subjectDto.getYearOfStudy(), subjectDto.getSemester());
	}
}
