package milos.savatijevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.service.StudentService;

public class StudentDtoFormatter implements Formatter<StudentDto>{
	
	private final StudentService studentService;
	@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto studentDto, Locale locale) {
		return studentDto.toString();
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		Long id=Long.parseLong(text);
		StudentDto studentDto = studentService.findById(id);
		return studentDto;
	}

}
