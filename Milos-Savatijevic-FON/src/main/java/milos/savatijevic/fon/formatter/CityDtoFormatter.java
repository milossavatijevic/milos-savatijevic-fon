package milos.savatijevic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.service.CityService;


public class CityDtoFormatter implements Formatter<CityDto>{
	private final CityService cityService;
	
	@Autowired
	public CityDtoFormatter(CityService cityService) {
		System.out.println("==========================================================================");
		System.out.println("=======CityDtoFormatter: constructor============================================");
		System.out.println("==========================================================================");
		this.cityService = cityService;
	}
	
	@Override
	public String print(CityDto cityDto, Locale locale) {
		System.out.println("==========================================================================");
		System.out.println("=======CityDtoFormatter: print============================================");
		System.out.println("==========================================================================");
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String city, Locale locale) {
		System.out.println("==========================================================================");
		System.out.println("=======CityDtoFormatter: parse============================================");
		System.out.println("==========================================================================");
		
		Long number=Long.parseLong(city);
		CityDto cityDto = cityService.findById(number);
		return cityDto;
	}

}
