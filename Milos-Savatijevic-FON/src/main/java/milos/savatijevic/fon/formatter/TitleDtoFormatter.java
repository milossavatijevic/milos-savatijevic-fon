package milos.savatijevic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.TitleDto;
import milos.savatijevic.fon.service.TitleService;

public class TitleDtoFormatter implements Formatter<TitleDto> {

	private final TitleService titleService;

	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		System.out.println("==========================================================================");
		System.out.println("=======TitleDtoFormatter: constructor============================================");
		System.out.println("==========================================================================");
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		System.out.println("==========================================================================");
		System.out.println("=======TitleDtoFormatter: print============================================");
		System.out.println("==========================================================================");
		return titleDto.toString();
	}

	@Override
	public TitleDto parse(String title, Locale locale) {
		System.out.println("==========================================================================");
		System.out.println("=======TitleDtoFormatter: parse============================================");
		System.out.println("==========================================================================");
		
		Long number=Long.parseLong(title);
		TitleDto titleDto = titleService.findById(number);
		return titleDto;
	}

}
