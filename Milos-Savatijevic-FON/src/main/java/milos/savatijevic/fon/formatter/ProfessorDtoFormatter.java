package milos.savatijevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.service.ProfessorService;

public class ProfessorDtoFormatter implements Formatter<ProfessorDto> {
	
	private final ProfessorService professorService;
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		this.professorService = professorService;
	}

	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		return professorDto.toString();
	}

	@Override
	public ProfessorDto parse(String text, Locale locale) throws ParseException {
		Long id=Long.parseLong(text);
		ProfessorDto professorDto = professorService.findById(id);
		return professorDto;
	}

}
