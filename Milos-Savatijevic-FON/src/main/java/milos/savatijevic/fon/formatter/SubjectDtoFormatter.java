package milos.savatijevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.service.SubjectService;

public class SubjectDtoFormatter implements Formatter<SubjectDto>{
	
	private final SubjectService subjectService;
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {
		return subjectDto.toString();
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		Long id=Long.parseLong(text);
		SubjectDto subjectDto = subjectService.findById(id);
		return subjectDto;
	}

}
