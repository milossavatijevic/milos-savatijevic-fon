package milos.savatijevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import milos.savatijevic.fon.enums.Semester;

public class SemesterFormatter implements Formatter<Semester>{

	@Override
	public String print(Semester semester, Locale locale) {
		System.out.println("==========================================================================");
		System.out.println("=======Semester: print============================================");
		System.out.println("==========================================================================");
		return semester.toString();
	}

	@Override
	public Semester parse(String semester, Locale locale) throws ParseException {
		System.out.println("==========================================================================");
		System.out.println("=======Semester: parse============================================");
		System.out.println("==========================================================================");
		
		for(Semester semesterEnum : Semester.values()) {
			if(semesterEnum.getSemester().equals(semester)) {
				return semesterEnum;
			}
		}
		
		return null;
	}

}
