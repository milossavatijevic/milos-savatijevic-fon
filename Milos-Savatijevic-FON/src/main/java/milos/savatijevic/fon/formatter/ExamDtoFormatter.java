package milos.savatijevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.service.ExamService;

public class ExamDtoFormatter implements Formatter<ExamDto>{
	
	private final ExamService examService;
	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		this.examService = examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		return examDto.toString();
	}

	@Override
	public ExamDto parse(String text, Locale locale) throws ParseException {
		Long id=Long.parseLong(text);
		ExamDto examDto = examService.findById(id);
		return examDto;
	}

}
