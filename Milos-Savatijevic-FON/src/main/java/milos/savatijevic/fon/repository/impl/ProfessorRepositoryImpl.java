package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.ProfessorEntity;
import milos.savatijevic.fon.repository.ProfessorRepository;

@Repository(value = "professorRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class ProfessorRepositoryImpl implements ProfessorRepository{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void save(ProfessorEntity professorEntity) {
		entityManager.persist(professorEntity);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfessorEntity> findAll() {
		return entityManager.createNamedQuery("findAllProfessors").getResultList();
	}

	@Override
	public void delete(ProfessorEntity professorEntity) throws DataIntegrityViolationException {
		ProfessorEntity entity = entityManager.find(ProfessorEntity.class, professorEntity.getId());
		try {
			entityManager.remove(entity);
		} catch (DataIntegrityViolationException | ConstraintViolationException ex) {
			throw ex;
		}
		
	}

	@Override
	public void update(ProfessorEntity professorEntity) {
		entityManager.merge(professorEntity);
		entityManager.flush();
	}

	@Override
	public ProfessorEntity findById(Long id) {
		return entityManager.find(ProfessorEntity.class, id);
	}

}
