package milos.savatijevic.fon.repository;

import java.util.List;

import milos.savatijevic.fon.entity.CityEntity;

public interface CityRepository {
	List<CityEntity> findAll();

	CityEntity findById(Long pttNumber);
}
