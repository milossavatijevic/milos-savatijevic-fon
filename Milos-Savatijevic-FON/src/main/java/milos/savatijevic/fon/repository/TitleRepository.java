package milos.savatijevic.fon.repository;

import java.util.List;

import milos.savatijevic.fon.entity.TitleEntity;

public interface TitleRepository {
	List<TitleEntity> findAll();

	TitleEntity findById(Long id);
}
