package milos.savatijevic.fon.repository;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.entity.StudentEntity;

public interface StudentRepository {
	void save(StudentEntity studentEntity);

	List<StudentEntity> getAll();

	void delete(StudentEntity studentEntity) throws DataIntegrityViolationException;

	void update(StudentEntity studentEntity);

	StudentEntity findById(Long id);
	

}
