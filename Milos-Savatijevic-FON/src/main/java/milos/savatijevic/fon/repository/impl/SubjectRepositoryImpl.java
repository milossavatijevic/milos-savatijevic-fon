package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.SubjectEntity;
import milos.savatijevic.fon.repository.SubjectRepository;

@Repository(value = "subjectRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class SubjectRepositoryImpl implements SubjectRepository{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void save(SubjectEntity subjectEntity) {
		entityManager.persist(subjectEntity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectEntity> findAll() {
		return entityManager.createNamedQuery("findAllSubjects").getResultList();
	}

	@Override
	public void delete(SubjectEntity subjectEntity) throws DataIntegrityViolationException {
		SubjectEntity entity = entityManager.find(SubjectEntity.class, subjectEntity.getId());
		try {
			entityManager.remove(entity);
		} catch (DataIntegrityViolationException | ConstraintViolationException ex) {
			throw ex;
		}
	}

	@Override
	public void update(SubjectEntity subjectEntity) {
		entityManager.merge(subjectEntity);
		entityManager.flush();
	}

	@Override
	public SubjectEntity findById(Long id) {
		return entityManager.find(SubjectEntity.class, id);
	}

}
