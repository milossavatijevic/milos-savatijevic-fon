package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.ApplicationEntity;
import milos.savatijevic.fon.repository.ApplicationRepository;

@Repository(value = "applicationRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class ApplicationRepositoryImpl implements ApplicationRepository{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void save(ApplicationEntity applicationEntity) {
		entityManager.persist(applicationEntity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApplicationEntity> findAll() {
		return entityManager.createNamedQuery("findAllApplications").getResultList();
	}

	@Override
	public ApplicationEntity findById(Long id) {
		return entityManager.find(ApplicationEntity.class, id);
	}

}
