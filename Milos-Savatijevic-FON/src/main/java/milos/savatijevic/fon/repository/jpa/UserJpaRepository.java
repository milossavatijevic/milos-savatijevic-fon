package milos.savatijevic.fon.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import milos.savatijevic.fon.entity.UserEntity;


@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, Long>{
	UserEntity findByUsername(String username);

}
