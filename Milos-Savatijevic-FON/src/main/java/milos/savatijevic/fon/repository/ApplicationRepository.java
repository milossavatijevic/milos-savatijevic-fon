package milos.savatijevic.fon.repository;

import java.util.List;

import milos.savatijevic.fon.entity.ApplicationEntity;

public interface ApplicationRepository {
	void save(ApplicationEntity ApplicationEntity);

	List<ApplicationEntity> findAll();

	ApplicationEntity findById(Long id);
}
