package milos.savatijevic.fon.repository;

import java.util.List;

import milos.savatijevic.fon.entity.ExamEntity;

public interface ExamRepository {

	void save(ExamEntity examEntity);

	List<ExamEntity> findAll();

	ExamEntity findById(Long id);

	List<ExamEntity> getByDateAndName(ExamEntity examEntity);

	List<ExamEntity> findNew();

}
