package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.CityEntity;
import milos.savatijevic.fon.repository.CityRepository;

@Repository(value = "cityRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class CityRepositoryImpl implements CityRepository{
	@PersistenceContext
	EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CityEntity> findAll() {
		List<CityEntity> cities = entityManager.createQuery("SELECT c FROM CityEntity c").getResultList();
		return cities;
	}

	@Override
	public CityEntity findById(Long pttNumber) {
		System.out.println("==========================================================================");
		System.out.println("==========================findById=========================");
		System.out.println("==========================================================================");
		return entityManager.find(CityEntity.class, pttNumber);
	}

}
