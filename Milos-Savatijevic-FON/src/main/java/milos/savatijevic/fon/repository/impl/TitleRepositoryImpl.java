package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.TitleEntity;
import milos.savatijevic.fon.repository.TitleRepository;

@Repository(value = "titleRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class TitleRepositoryImpl implements TitleRepository{
	
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<TitleEntity> findAll() {
		List<TitleEntity> titles = entityManager.createQuery("SELECT t FROM TitleEntity t").getResultList();
		return titles;
	}

	@Override
	public TitleEntity findById(Long id) {
		System.out.println("==========================================================================");
		System.out.println("==========================findById=========================");
		System.out.println("==========================================================================");
		return entityManager.find(TitleEntity.class, id);
	}
	

}
