package milos.savatijevic.fon.repository;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.entity.ProfessorEntity;


public interface ProfessorRepository {
	void save(ProfessorEntity professorEntity);

	List<ProfessorEntity> findAll();

	void delete(ProfessorEntity professorEntity) throws DataIntegrityViolationException;

	void update(ProfessorEntity professorEntity);

	ProfessorEntity findById(Long id);
}
