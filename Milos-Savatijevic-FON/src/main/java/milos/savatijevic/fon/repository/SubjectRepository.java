package milos.savatijevic.fon.repository;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.entity.SubjectEntity;

public interface SubjectRepository {

	void save(SubjectEntity subjectEntity);

	List<SubjectEntity> findAll();

	void delete(SubjectEntity subjectEntity) throws DataIntegrityViolationException;

	void update(SubjectEntity subjectEntity);

	SubjectEntity findById(Long id);
}
