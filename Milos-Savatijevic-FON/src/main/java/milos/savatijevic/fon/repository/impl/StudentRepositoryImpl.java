package milos.savatijevic.fon.repository.impl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.StudentEntity;
import milos.savatijevic.fon.repository.StudentRepository;

@Repository(value = "studentRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class StudentRepositoryImpl implements StudentRepository{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public void save(StudentEntity studentEntity) {
		entityManager.persist(studentEntity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentEntity> getAll() {
		return entityManager.createNamedQuery("findAllStudents").getResultList();
	}

	@Override
	public void delete(StudentEntity studentEntity) throws DataIntegrityViolationException {
		StudentEntity entity = entityManager.find(StudentEntity.class, studentEntity.getId());
		try {
			entityManager.remove(entity);
		} catch (DataIntegrityViolationException | ConstraintViolationException ex) {
			throw ex;
		}
		
	}

	@Override
	public void update(StudentEntity studentEntity) {
		entityManager.merge(studentEntity);
		entityManager.flush();
	}

	@Override
	public StudentEntity findById(Long id) {
		return entityManager.find(StudentEntity.class, id);
	}


}
