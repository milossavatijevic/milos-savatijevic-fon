package milos.savatijevic.fon.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.entity.ExamEntity;
import milos.savatijevic.fon.repository.ExamRepository;

@Repository(value = "examRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class ExamRepositoryImpl implements ExamRepository{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public void save(ExamEntity examEntity) {
		entityManager.persist(examEntity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ExamEntity> findAll() {
		return entityManager.createNamedQuery("findAllExams").getResultList();
	}

	@Override
	public ExamEntity findById(Long id) {
		return entityManager.find(ExamEntity.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ExamEntity> getByDateAndName(ExamEntity examEntity) {
		Query query = entityManager.createNamedQuery("findExamByDateAndSubject");
		query.setParameter("subject", examEntity.getSubjectEntity());
		List<ExamEntity> exam = query.getResultList();
		return exam;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ExamEntity> findNew() {
		return entityManager.createNamedQuery("findNewExams").getResultList();
	}

}
