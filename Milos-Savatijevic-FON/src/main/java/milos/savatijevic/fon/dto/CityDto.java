package milos.savatijevic.fon.dto;

import java.io.Serializable;

public class CityDto implements Serializable{

	private static final long serialVersionUID = -3643231992182324393L;
	
	private Long pttNumber;
	private String name;
	
	public CityDto(Long pttNumber, String name) {
		this.pttNumber = pttNumber;
		this.name = name;
	}

	public CityDto() {
		
	}

	public Long getPttNumber() {
		return pttNumber;
	}

	public void setPttNumber(Long pttNumber) {
		this.pttNumber = pttNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityDto [pttNumber=" + pttNumber + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pttNumber == null) ? 0 : pttNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CityDto other = (CityDto) obj;
		if (pttNumber == null) {
			if (other.pttNumber != null)
				return false;
		} else if (!pttNumber.equals(other.pttNumber))
			return false;
		return true;
	}
	
	

}
