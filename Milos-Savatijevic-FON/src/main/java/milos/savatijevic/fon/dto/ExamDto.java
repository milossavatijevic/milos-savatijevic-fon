package milos.savatijevic.fon.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

public class ExamDto implements Serializable{

	private static final long serialVersionUID = -7526418609939687628L;
	
	private Long id;
	@NotBlank(message = "cannot be empty")
	private String examDate;
	private SubjectDto subjectDto;
	private ProfessorDto professorDto;
	
	public ExamDto() {
		
	}

	public ExamDto(Long id, String examDate, SubjectDto subjectDto, ProfessorDto professorDto) {
		super();
		this.id = id;
		this.examDate = examDate;
		this.subjectDto = subjectDto;
		this.professorDto = professorDto;
	}
	
	public String getSubjectDate() {
		return subjectDto.getName()+" ("+examDate+")";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", examDate=" + examDate + ", subjectDto=" + subjectDto + ", professorDto="
				+ professorDto + "]";
	}

	public SubjectDto getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}

	public ProfessorDto getProfessorDto() {
		return professorDto;
	}

	public void setProfessorDto(ProfessorDto professorDto) {
		this.professorDto = professorDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examDate == null) ? 0 : examDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDto other = (ExamDto) obj;
		if (examDate == null) {
			if (other.examDate != null)
				return false;
		} else if (!examDate.equals(other.examDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
