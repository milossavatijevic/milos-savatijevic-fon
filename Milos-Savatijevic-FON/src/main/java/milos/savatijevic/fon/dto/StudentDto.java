package milos.savatijevic.fon.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import milos.savatijevic.fon.annotation.PhoneNumber;

public class StudentDto implements Serializable{

	private static final long serialVersionUID = 6764607875307586973L;
	
	private Long id;
	@NotBlank(message = "cannot be empty")
	private String indexNumber;
	@NotBlank(message = "cannot be empty")
	private String firstname;
	@NotBlank(message = "cannot be empty")
	private String lastname;
	@Size(max = 30, message = "email cannot be longer than 30 characters")
	private String email;
	private String address;
	@PhoneNumber(message = "cannot contains special characters and letters")
	private String phone;
	private Long currentYearOfStudy;
	private CityDto cityDto;
	
	public StudentDto(Long id, String indexNumber, String firstname, String lastname, String email, String address, String phone,
			Long currentYearOfStudy, CityDto cityDto) {
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
		this.cityDto = cityDto;
	}

	public StudentDto() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", email=" + email + ", address=" + address + ", phone=" + phone + ", currentYearOfStudy="
				+ currentYearOfStudy + ", cityDto=" + cityDto + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDto other = (StudentDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		return true;
	}
	
	

}
