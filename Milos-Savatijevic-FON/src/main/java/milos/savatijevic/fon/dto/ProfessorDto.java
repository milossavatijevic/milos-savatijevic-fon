package milos.savatijevic.fon.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import milos.savatijevic.fon.annotation.PhoneNumber;

public class ProfessorDto implements Serializable {

	private static final long serialVersionUID = 5979883076712774123L;

	private Long id;
	@NotBlank(message = "cannot be empty")
	private String firstname;
	@NotBlank(message = "cannot be empty")
	private String lastname;
	@Size(max = 30, message = "email cannot be longer than 30 characters")
	private String email;
	private String address;
	@PhoneNumber(message = "cannot contains special characters and letters")
	private String phone;
	@NotBlank(message = "cannot be empty")
	private String reelectionDate;
	private CityDto cityDto;
	private TitleDto titleDto;
	
	public ProfessorDto() {
		
	}

	public ProfessorDto(Long id, String firstname, String lastname, String email, String address, String phone,
			String reelectionDate, CityDto cityDto, TitleDto titleDto) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.cityDto = cityDto;
		this.titleDto = titleDto;
	}
	
	public String getFullname() {
		return firstname+" "+lastname;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(String reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", address=" + address + ", phone=" + phone + ", reelectionDate=" + reelectionDate + ", cityDto="
				+ cityDto + ", titleDto=" + titleDto + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorDto other = (ProfessorDto) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
