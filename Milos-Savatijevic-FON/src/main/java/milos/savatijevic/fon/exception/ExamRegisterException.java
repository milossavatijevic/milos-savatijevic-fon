package milos.savatijevic.fon.exception;

public class ExamRegisterException extends Exception{

	private static final long serialVersionUID = 7866612782634486383L;
	
	public ExamRegisterException(String message) {
		super(message);
	}

}
