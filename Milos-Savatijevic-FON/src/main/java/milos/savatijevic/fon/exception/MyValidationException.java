package milos.savatijevic.fon.exception;

public class MyValidationException  extends Exception{

	private static final long serialVersionUID = 7685950201271904338L;

	public MyValidationException(String message) {
		super(message);
	}

}
