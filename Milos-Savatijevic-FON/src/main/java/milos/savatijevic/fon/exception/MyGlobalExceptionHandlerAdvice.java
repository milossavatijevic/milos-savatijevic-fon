package milos.savatijevic.fon.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
@ControllerAdvice
public class MyGlobalExceptionHandlerAdvice {
	@ExceptionHandler(MyValidationException.class)
	public String examExceptionHandler(MyValidationException myValidationException,RedirectAttributes redirectAttributes) {
		
		System.out.println("====================================================================");
		System.out.println("@ControllerAdvice exception ocured: MyValidationException===========");
		System.out.println("====================================================================");
		
		redirectAttributes.addFlashAttribute("errorMessage", myValidationException.getMessage());
		
		return "redirect:/exam/add";
	}
	
	@ExceptionHandler(ExamRegisterException.class)
	public String applicationExceptionHandler(ExamRegisterException examRegisterException,RedirectAttributes redirectAttributes) {
		
		System.out.println("====================================================================");
		System.out.println("@ControllerAdvice exception ocured: ExamRegisterException===========");
		System.out.println("====================================================================");
		
		redirectAttributes.addFlashAttribute("errorMessage", examRegisterException.getMessage());
		
		return "redirect:/application/add";
	}
	
	
	
	
}
