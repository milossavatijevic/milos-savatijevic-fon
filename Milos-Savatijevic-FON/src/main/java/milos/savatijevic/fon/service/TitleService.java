package milos.savatijevic.fon.service;

import java.util.List;

import milos.savatijevic.fon.dto.TitleDto;

public interface TitleService {
	List<TitleDto> getAll();

	TitleDto findById(Long id);
}
