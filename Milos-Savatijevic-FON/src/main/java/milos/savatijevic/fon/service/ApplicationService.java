package milos.savatijevic.fon.service;

import java.util.List;

import milos.savatijevic.fon.dto.ApplicationDto;
import milos.savatijevic.fon.exception.ExamRegisterException;

public interface ApplicationService {
	void save(ApplicationDto applicationDto) throws ExamRegisterException;

	List<ApplicationDto> getAll();

	ApplicationDto findById(Long id);
}
