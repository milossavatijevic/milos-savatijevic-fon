package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.ExamConverter;
import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.entity.ExamEntity;
import milos.savatijevic.fon.exception.MyValidationException;
import milos.savatijevic.fon.repository.ExamRepository;
import milos.savatijevic.fon.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService{
	
	@Qualifier(value = "examRepository")
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private ExamConverter examConverter;

	@Override
	public void save(ExamDto examDto) throws MyValidationException {
		
		ExamEntity examEntity = examConverter.dtoToEntity(examDto);
		
		if(examEntity.getExamDate().before(new Date()))
			throw new MyValidationException("cannot be set date from past");
		
		List<ExamEntity> existExam = examRepository.getByDateAndName(examEntity);
		if(!existExam.isEmpty())
			throw new MyValidationException("exam is already registered");
		
		examRepository.save(examEntity);
	}
	

	@Override
	public List<ExamDto> getAll() {
		List<ExamDto> exams = new ArrayList<ExamDto>();
		List<ExamEntity> examsEntity = examRepository.findAll();
		
		for (ExamEntity exam : examsEntity) {
			exams.add(examConverter.entityToDto(exam));
		}
		return exams;
	}

	@Override
	public ExamDto findById(Long id) {
		ExamEntity examEntity = examRepository.findById(id);
		ExamDto examDto = examConverter.entityToDto(examEntity);
		return examDto;
	}


	@Override
	public List<ExamDto> getNewExams() {
		List<ExamDto> exams = new ArrayList<ExamDto>();
		List<ExamEntity> examsEntity = examRepository.findNew();
		
		for (ExamEntity exam : examsEntity) {
			exams.add(examConverter.entityToDto(exam));
		}
		return exams;
	}

}
