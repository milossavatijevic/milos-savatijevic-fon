package milos.savatijevic.fon.service;


import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.dto.StudentDto;

public interface StudentService {
	void save(StudentDto studentDto);
	
	List<StudentDto> getAll();

	void delete(StudentDto studentDto) throws DataIntegrityViolationException;

	void update(StudentDto studentDto);

	StudentDto findById(Long id);
}
