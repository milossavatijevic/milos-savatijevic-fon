package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.SubjectConverter;
import milos.savatijevic.fon.dto.SubjectDto;
import milos.savatijevic.fon.entity.SubjectEntity;
import milos.savatijevic.fon.repository.SubjectRepository;
import milos.savatijevic.fon.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
	
	@Qualifier(value = "subjectRepository")
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private SubjectConverter subjectConverter;

	@Override
	public void save(SubjectDto subjectDto) {
		SubjectEntity subjectEntity = subjectConverter.dtoToEntity(subjectDto);
		subjectRepository.save(subjectEntity);
	}

	@Override
	public List<SubjectDto> getAll() {
		List<SubjectDto> subjects = new ArrayList<SubjectDto>();
		List<SubjectEntity> subjectsEntity = subjectRepository.findAll();
		for (SubjectEntity subject : subjectsEntity) {
			subjects.add(subjectConverter.entityToDto(subject));
		}
		return subjects;
	}

	@Override
	public void delete(SubjectDto subjectDto) throws DataIntegrityViolationException {
		SubjectEntity subjectEntity = subjectConverter.dtoToEntity(subjectDto);
		subjectRepository.delete(subjectEntity);
	}

	@Override
	public void update(SubjectDto subjectDto) {
		SubjectEntity subjectEntity = subjectConverter.dtoToEntity(subjectDto);
		subjectRepository.update(subjectEntity);
	}

	@Override
	public SubjectDto findById(Long id) {
		SubjectEntity subjectEntity = subjectRepository.findById(id);
		SubjectDto subjectDto = subjectConverter.entityToDto(subjectEntity);
		return subjectDto;
	}
	
	
}
