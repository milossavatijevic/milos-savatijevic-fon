package milos.savatijevic.fon.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.ApplicationConverter;
import milos.savatijevic.fon.dto.ApplicationDto;
import milos.savatijevic.fon.entity.ApplicationEntity;
import milos.savatijevic.fon.exception.ExamRegisterException;
import milos.savatijevic.fon.repository.ApplicationRepository;
import milos.savatijevic.fon.service.ApplicationService;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService{
	
	@Qualifier(value = "applicationRepository")
	@Autowired
	private ApplicationRepository applicationRepository;
	@Autowired
	private ApplicationConverter applicationConverter;

	@Override
	public void save(ApplicationDto applicationDto) throws ExamRegisterException {
		ApplicationEntity applicationEntity = applicationConverter.dtoToEntity(applicationDto);
		
		LocalDate chosenDate= new Date(applicationEntity.getExamEntity().getExamDate().getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if(Period.between(LocalDate.now(), chosenDate).getDays() > 7)
			throw new ExamRegisterException("exam cannot be registered more than one week before exam");
		
		Long subjectYear = applicationEntity.getExamEntity().getSubjectEntity().getYearOfStudy();
		Long studentYear = applicationEntity.getStudentEntity().getCurrentYearOfStudy();
		if(subjectYear>studentYear)
			throw new ExamRegisterException("student has not listened this subject yet");
		
		
		applicationRepository.save(applicationEntity);
	}

	@Override
	public List<ApplicationDto> getAll() {
		List<ApplicationDto> applications = new ArrayList<ApplicationDto>();
		List<ApplicationEntity> applicationsEntity = applicationRepository.findAll();
		
		for (ApplicationEntity application : applicationsEntity) {
			applications.add(applicationConverter.entityToDto(application));
		}
		return applications;
	}

	@Override
	public ApplicationDto findById(Long id) {
		ApplicationEntity applicationEntity = applicationRepository.findById(id);
		ApplicationDto applicationDto = applicationConverter.entityToDto(applicationEntity);
		return applicationDto;
	}

}
