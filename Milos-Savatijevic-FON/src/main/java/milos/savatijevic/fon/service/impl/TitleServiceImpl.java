package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.TitleConverter;
import milos.savatijevic.fon.dto.TitleDto;
import milos.savatijevic.fon.entity.TitleEntity;
import milos.savatijevic.fon.repository.TitleRepository;
import milos.savatijevic.fon.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService{
	
	@Qualifier(value = "titleRepository")
	@Autowired
	private TitleRepository titleRepository;
	@Autowired
	private TitleConverter titleConverter;

	@Override
	public List<TitleDto> getAll() {
		List<TitleDto> titles = new ArrayList<TitleDto>();
		List<TitleEntity> titlesEntity = titleRepository.findAll();
		for (TitleEntity titleEntity : titlesEntity) {
			titles.add(titleConverter.entityToDto(titleEntity));
		}
		return titles;
	}

	@Override
	public TitleDto findById(Long id) {
		TitleEntity titleEntity = titleRepository.findById(id);
		TitleDto titleDto = titleConverter.entityToDto(titleEntity);
		return titleDto;
	}

}
