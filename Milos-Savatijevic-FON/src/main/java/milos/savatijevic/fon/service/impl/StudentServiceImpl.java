package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.StudentConverter;
import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.entity.StudentEntity;
import milos.savatijevic.fon.repository.StudentRepository;
import milos.savatijevic.fon.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	@Qualifier(value = "studentRepository")
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private StudentConverter studentConverter;

	@Override
	public void save(StudentDto studentDto) {
		StudentEntity studentEntity = studentConverter.dtoToEntity(studentDto);
		studentRepository.save(studentEntity);
	}

	@Override
	public List<StudentDto> getAll() {
		List<StudentDto> students = new ArrayList<StudentDto>();
		List<StudentEntity> studentsEntity = studentRepository.getAll();
		for (StudentEntity student : studentsEntity) {
			students.add(studentConverter.entityToDto(student));
		}
		return students;
	}

	@Override
	public void delete(StudentDto studentDto) throws DataIntegrityViolationException {
		StudentEntity studentEntity = studentConverter.dtoToEntity(studentDto);
		studentRepository.delete(studentEntity);
	}

	@Override
	public void update(StudentDto studentDto) {
		StudentEntity studentEntity = studentConverter.dtoToEntity(studentDto);
		studentRepository.update(studentEntity);
	}

	@Override
	public StudentDto findById(Long id) {
		StudentEntity studentEntity = studentRepository.findById(id);
		StudentDto studentDto = studentConverter.entityToDto(studentEntity);
		return studentDto;
	}
}
