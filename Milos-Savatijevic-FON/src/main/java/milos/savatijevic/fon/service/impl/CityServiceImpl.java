package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.CityConverter;
import milos.savatijevic.fon.dto.CityDto;
import milos.savatijevic.fon.entity.CityEntity;
import milos.savatijevic.fon.repository.CityRepository;
import milos.savatijevic.fon.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService{
	
	@Qualifier(value = "cityRepository")
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private CityConverter cityConverter;

	@Override
	public List<CityDto> getAll() {
		List<CityDto> cities = new ArrayList<CityDto>();
		List<CityEntity> citiesEntity = cityRepository.findAll();
		for (CityEntity cityEntity : citiesEntity) {
			cities.add(cityConverter.entityToDto(cityEntity));
		}
		return cities;
	}

	@Override
	public CityDto findById(Long pttNumber) {
		CityEntity cityEntity = cityRepository.findById(pttNumber);
		CityDto cityDto = cityConverter.entityToDto(cityEntity);
		return cityDto;
	}

}
