package milos.savatijevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import milos.savatijevic.fon.converter.ProfessorConverter;
import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.entity.ProfessorEntity;
import milos.savatijevic.fon.repository.ProfessorRepository;
import milos.savatijevic.fon.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService{
	
	@Qualifier(value = "professorRepository")
	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private ProfessorConverter professorConverter;

	@Override
	public void save(ProfessorDto professorDto) {
		ProfessorEntity professorEntity = professorConverter.dtoToEntity(professorDto);
		professorRepository.save(professorEntity);
	}

	@Override
	public List<ProfessorDto> getAll() {
		List<ProfessorDto> professors = new ArrayList<ProfessorDto>();
		List<ProfessorEntity> professorsEntity = professorRepository.findAll();
		for (ProfessorEntity professor : professorsEntity) {
			professors.add(professorConverter.entityToDto(professor));
		}
		return professors;
	}

	@Override
	public void delete(ProfessorDto professorDto) throws DataIntegrityViolationException {
		ProfessorEntity professorEntity = professorConverter.dtoToEntity(professorDto);
		professorRepository.delete(professorEntity);
	}

	@Override
	public void update(ProfessorDto professorDto) {
		ProfessorEntity professorEntity = professorConverter.dtoToEntity(professorDto);
		professorRepository.update(professorEntity);
	}

	@Override
	public ProfessorDto findById(Long id) {
		ProfessorEntity professorEntity = professorRepository.findById(id);
		ProfessorDto professorDto = professorConverter.entityToDto(professorEntity);
		return professorDto;
	}

}
