package milos.savatijevic.fon.service;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.dto.ProfessorDto;

public interface ProfessorService {

	void save(ProfessorDto professorDto);

	List<ProfessorDto> getAll();

	void delete(ProfessorDto professorDto) throws DataIntegrityViolationException;

	void update(ProfessorDto professorDto);

	ProfessorDto findById(Long id);
}
