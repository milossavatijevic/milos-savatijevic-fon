package milos.savatijevic.fon.service;

import java.util.List;

import milos.savatijevic.fon.dto.CityDto;

public interface CityService {
	List<CityDto> getAll();

	CityDto findById(Long pttNumber);
}
