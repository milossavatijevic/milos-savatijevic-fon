package milos.savatijevic.fon.service;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import milos.savatijevic.fon.dto.SubjectDto;

public interface SubjectService {

	void save(SubjectDto subjectDto);

	List<SubjectDto> getAll();

	void delete(SubjectDto subjectDto) throws DataIntegrityViolationException;

	void update(SubjectDto subjectDto);

	SubjectDto findById(Long id);
	
	
}
