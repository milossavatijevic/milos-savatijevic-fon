package milos.savatijevic.fon.service;

import java.util.List;

import milos.savatijevic.fon.dto.ExamDto;
import milos.savatijevic.fon.exception.MyValidationException;

public interface ExamService {

	void save(ExamDto examDto) throws MyValidationException;

	List<ExamDto> getAll();

	ExamDto findById(Long id);

	List<ExamDto> getNewExams();
}
