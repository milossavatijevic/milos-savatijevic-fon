package milos.savatijevic.fon.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import milos.savatijevic.fon.annotation.PhoneNumber;

public class PhoneValidator implements ConstraintValidator<PhoneNumber, String>{

	@Override
	public void initialize(PhoneNumber constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value==null || value.matches("[0-9]+");
	}

}
