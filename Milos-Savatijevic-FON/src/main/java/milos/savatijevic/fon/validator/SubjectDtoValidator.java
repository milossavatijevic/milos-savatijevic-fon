package milos.savatijevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.savatijevic.fon.dto.SubjectDto;

public class SubjectDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return SubjectDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SubjectDto subjectDto = (SubjectDto) target;

		if ((subjectDto.getName().length() < 3 || subjectDto.getName().length() > 30)
				&& subjectDto.getName().length() != 0) {
			errors.rejectValue("name", "subject.name.size",
					"name has to be between 3 and 30 characters long");
		}

		Long year = subjectDto.getYearOfStudy();
		
		if(!(year==1 || year==2 || year==3 || year==4 || year==5)) {
			errors.rejectValue("yearOfStudy", "subject.yearOfStudy.year",
					"year has to be in range from 1-5");
		}
		

	}

}
