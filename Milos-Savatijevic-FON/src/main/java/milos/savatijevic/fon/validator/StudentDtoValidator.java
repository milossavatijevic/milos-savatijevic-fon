package milos.savatijevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.savatijevic.fon.dto.StudentDto;
import milos.savatijevic.fon.service.StudentService;

public class StudentDtoValidator implements Validator {
	
	private StudentService studentService;
	
	public StudentDtoValidator(StudentService studentService) {
		this.studentService = studentService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;

		if((studentDto.getIndexNumber().length()<10 || studentDto.getIndexNumber().length()>10) && studentDto.getIndexNumber().length()!=0) {
			errors.rejectValue("indexNumber", "student.indexNumber.size",
					"index number has to be 10 characters long");
		}
		
		for(StudentDto student : studentService.getAll()) {
			if(studentDto.getId()==null && studentDto.getIndexNumber().equalsIgnoreCase(student.getIndexNumber())) {
				errors.rejectValue("indexNumber", "student.indexNumber.unique.add",
						"index number already exists");
			}
			if(studentDto.getId()!=null && !studentDto.getId().equals(student.getId()) 
					&& studentDto.getIndexNumber().equalsIgnoreCase(student.getIndexNumber())) {
				errors.rejectValue("indexNumber", "student.indexNumber.unique.edit",
						"index number already exists");
			}
		}
		
		if((studentDto.getFirstname().length()<3 || studentDto.getFirstname().length()>30) && studentDto.getFirstname().length()!=0) {
			errors.rejectValue("firstname", "student.firstname.size",
					"firstname has to be between 3 and 30 characters long");
		}
		
		if((studentDto.getLastname().length()<3 || studentDto.getLastname().length()>30) && studentDto.getLastname().length()!=0) {
			errors.rejectValue("lastname", "student.lastname.size",
					"lastname has to be between 3 and 30 characters long");
		}
		
		for(StudentDto student : studentService.getAll()) {
			if(studentDto.getId()==null && studentDto.getEmail().equalsIgnoreCase(student.getEmail())) {
				errors.rejectValue("email", "student.email.unique.add",
						"email already exists");
			}
			if(studentDto.getId()!=null && !studentDto.getId().equals(student.getId()) 
					&& studentDto.getEmail().equalsIgnoreCase(student.getEmail())) {
				errors.rejectValue("email", "student.email.unique.edit",
						"email already exists");
			}
		}
		
		if(studentDto.getEmail().indexOf("@")==-1 && studentDto.getEmail().length()<=30) {
			errors.rejectValue("email", "student.email.character",
					"email has to include @ character");
		}
		
		if(studentDto.getAddress().length()<3 || studentDto.getAddress().length()>50) {
			errors.rejectValue("address", "student.address.size",
					"address has to be between 3 and 50 characters long");
		}
		
		if(studentDto.getPhone().length()<6 || studentDto.getPhone().length()>15) {
			errors.rejectValue("phone", "student.phone.size",
					"phone has to be between 6 and 15 digits long");
		}
		
		Long year = studentDto.getCurrentYearOfStudy();
		
		if(!(year==1 || year==2 || year==3 || year==4 || year==5)) {
			errors.rejectValue("currentYearOfStudy", "student.currentYearOfStudy.year",
					"year has to be in range from 1-5");
		}
		
		
		
	}

}
