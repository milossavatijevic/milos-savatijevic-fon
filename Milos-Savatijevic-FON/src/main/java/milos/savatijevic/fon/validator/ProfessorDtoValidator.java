package milos.savatijevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.savatijevic.fon.dto.ProfessorDto;
import milos.savatijevic.fon.service.ProfessorService;

public class ProfessorDtoValidator implements Validator {
	
	private ProfessorService professorService;

	public ProfessorDtoValidator(ProfessorService professorService) {
		this.professorService = professorService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProfessorDto professorDto = (ProfessorDto) target;

		
		if((professorDto.getFirstname().length()<3 || professorDto.getFirstname().length()>30) && professorDto.getFirstname().length()!=0) {
			errors.rejectValue("firstname", "professor.firstname.size",
					"firstname has to be between 3 and 30 characters long");
		}
		
		if((professorDto.getLastname().length()<3 || professorDto.getLastname().length()>30) && professorDto.getLastname().length()!=0) {
			errors.rejectValue("lastname", "professor.lastname.size",
					"lastname has to be between 3 and 30 characters long");
		}
		
		for(ProfessorDto professor : professorService.getAll()) {
			if(professorDto.getId()==null && professorDto.getEmail().equalsIgnoreCase(professor.getEmail())) {
				errors.rejectValue("email", "professor.email.unique.add",
						"email already exists");
			}
			if(professorDto.getId()!=null && !professorDto.getId().equals(professor.getId()) 
					&& professorDto.getEmail().equalsIgnoreCase(professor.getEmail())) {
				errors.rejectValue("email", "professor.email.unique.edit",
						"email already exists");
			}
		}
		
		if(professorDto.getEmail().indexOf("@")==-1 && professorDto.getEmail().length()<=30) {
			errors.rejectValue("email", "professor.email.character",
					"email has to include @ character");
		}
		
		if(professorDto.getAddress().length()<3 || professorDto.getAddress().length()>50) {
			errors.rejectValue("address", "professor.address.size",
					"address has to be between 3 and 50 characters long");
		}
		
		if(professorDto.getPhone().length()<6 || professorDto.getPhone().length()>15) {
			errors.rejectValue("phone", "professor.phone.size",
					"phone has to be between 6 and 15 digits long");
		}
		
	}
	
	
}
