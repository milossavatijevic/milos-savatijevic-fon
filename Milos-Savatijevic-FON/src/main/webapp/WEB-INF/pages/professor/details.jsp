<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
	<div>
		<c:url value="/professor" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>
	<div class="container fill">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">${professorDto.firstname}&ensp;${professorDto.lastname}</h2>
						<table class="table table-hover">
							<tr>
								<td><b>Firstname</b></td>
								<td>${professorDto.firstname}</td>
							</tr>
							<tr>
								<td><b>Lastname</b></td>
								<td>${professorDto.lastname}</td>
							</tr>
							<tr>
								<td><b>Title</b></td>
								<td>${professorDto.titleDto.name}</td>
							</tr>
							<tr>
								<td><b>Email</b></td>
								<td>${professorDto.email}</td>
							</tr>
							<tr>
								<td><b>Address</b></td>
								<td>${professorDto.address}</td>
							</tr>
							<tr>
								<td><b>Phone</b></td>
								<td>${professorDto.phone}</td>
							</tr>
							<tr>
								<td><b>Reelection date</b></td>
								<td>${professorDto.reelectionDate}</td>
							</tr>
							<tr>
								<td><b>City</b></td>
								<td>${professorDto.cityDto.name}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
