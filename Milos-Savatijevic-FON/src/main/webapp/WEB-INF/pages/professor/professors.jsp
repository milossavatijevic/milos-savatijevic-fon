<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Professors</title>
<style>
#container {
	height: 70vh;
}

.error {
	color: red;
}
</style>
</head>
<body>
	<div>
		<c:url value="/home" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>
	<nav class="navbar navbar-expand-lg navbar-dark bg-light">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav mx-auto">
					<c:url value="/professor/add" var="addProfessors" />
					<li class="nav-item "><a class="nav-link text-primary"
						href="<c:out value="${addProfessors}"/>">Add new professor</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Professors</h2>
						<c:if test="${not empty errorMessage}">
							<div class="error">${errorMessage}</div>
						</c:if>
						<table class="table table-hover" id="professors">
							<thead>
								<tr>
									<th scope="col">Firstname</th>
									<th scope="col">Lastname</th>
									<th scope="col">Title</th>
									<td colspan="3"></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${professors}" var="professor">

									<c:url value="/professor/details" var="detailsProfessor">
										<c:param name="id" value="${professor.id}" />
									</c:url>
									<c:url value="/professor/edit" var="editProfessor">
										<c:param name="id" value="${professor.id}" />
									</c:url>
									<c:url value="/professor/delete" var="deleteProfessor">
										<c:param name="id" value="${professor.id}" />
									</c:url>
									<tr>
										<td>${professor.firstname}</td>
										<td>${professor.lastname}</td>
										<td>${professor.titleDto.name}</td>
										<td><a href="<c:out value = "${detailsProfessor}"/>">Details</a></td>
										<td><a href="<c:out value = "${editProfessor}"/>">Edit</a></td>
										<td><a href="<c:out value = "${deleteProfessor}"/>">Delete</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<nav>
							<div id="pagination" class="pagination justify-content-center"
								style="flex-wrap: wrap;">
								<div class="page-item page-link">

									<c:forEach begin="1" end="${maxPages}" varStatus="i">
										<c:url value="/professor" var="url">
											<c:param name="page" value="${i.index}" />
										</c:url>
										<c:choose>
											<c:when test="${page == i.index}">
												<span>${i.index}</span>
											</c:when>
											<c:otherwise>
												<a href='<c:out value="${url}" />'>${i.index}</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
