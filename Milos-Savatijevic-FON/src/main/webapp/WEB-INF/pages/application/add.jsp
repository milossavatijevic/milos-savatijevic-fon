<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Apply exam</title>
<style>
.error {
	color: red;
}
	#container {
	height: 70vh;
}
</style>
</head>
<body>
	<div>
		<c:url value="/application" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>
	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-7 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Register exam</h2>
						<c:set value="${pageContext.request.contextPath}"
							var="contextPath"></c:set>
						<form:form method="post" action="${contextPath}/application/save"
							modelAttribute="applicationDto" class="form-horizontal">
							<fieldset>
								<c:if test="${not empty errorMessage}">
									<div class="error">${errorMessage}</div>
								</c:if>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="studentDto">Student</form:label>
									<div class="col-sm-8">
										<form:select class="form-control" path="studentDto">
											<form:options items="${students}" itemValue="id"
												itemLabel="indexNumber" />
										</form:select>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="examDto">Exam</form:label>
									<div class="col-sm-8">
										<form:select class="form-control" path="examDto">
											<form:options items="${exams}" itemValue="id"
												itemLabel="subjectDate" />
										</form:select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4">
										<button class="btn btn-primary" id="save" name="operation"
											value="save">Save</button>
									</div>
								</div>
							</fieldset>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>