<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Subjects</title>
<style>
#container {
	height: 70vh;
}

.error {
	color: red;
}
</style>
</head>
<body>
	<div>
		<c:url value="/home" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark bg-light">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav mx-auto">
					<c:url value="/subject/add" var="addSubjects" />
					<li class="nav-item "><a class="nav-link text-primary"
						href="<c:out value="${addSubjects}"/>">Add new subject</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Subjects</h2>
						<c:if test="${not empty errorMessage}">
							<div class="error">${errorMessage}</div>
						</c:if>
						<%-- <div class="row">
							<div class="col-sm-12 col-md-6" style="">
								<div id="num">
									<form action="/Milos-Savatijevic-FON/subject">
										<label style="display: flex; align-items: center;">Show
											<select name="num" class="form-control form-control-sm"
											style="margin: 0 1em;">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="5">5</option>
										</select> entries <input class="ml-3 bg-primary" type="submit"
											value="Go">
										</label>
									</form>
								</div>
							</div>
						</div> --%>
						<table class="table table-hover" id="subjects">
							<thead>
								<tr>
									<th scope="col">Name</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${subjects}" var="subject">

									<c:url value="/subject/details" var="detailsSubject">
										<c:param name="id" value="${subject.id}" />
									</c:url>
									<c:url value="/subject/edit" var="editSubject">
										<c:param name="id" value="${subject.id}" />
									</c:url>
									<c:url value="/subject/delete" var="deleteSubject">
										<c:param name="id" value="${subject.id}" />
									</c:url>
									<tr>
										<td>${subject.name}</td>
										<td><a href="<c:out value = "${detailsSubject}"/>">Details</a></td>
										<td><a href="<c:out value = "${editSubject}"/>">Edit</a></td>
										<td><a href="<c:out value = "${deleteSubject}"/>">Delete</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<nav>
							<div id="pagination" class="pagination justify-content-center"
								style="flex-wrap: wrap;">
								<div class="page-item page-link">

									<c:forEach begin="1" end="${maxPages}" varStatus="i">
										<c:url value="/subject" var="url">
											<c:param name="page" value="${i.index}" />
										</c:url>
										<c:choose>
											<c:when test="${page == i.index}">
												<span>${i.index}</span>
											</c:when>
											<c:otherwise>
												<a href='<c:out value="${url}" />'>${i.index}</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
