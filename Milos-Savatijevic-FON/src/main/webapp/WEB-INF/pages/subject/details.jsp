<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
<style>
	#container{
		height: 70vh;
	}
</style>
</head>
<body>
	<div>
		<c:url value="/subject" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>
	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">${subjectDto.name}</h2>
						<table class="table table-hover">
							<tr>
								<td><b>Name</b></td>
								<td>${subjectDto.name}</td>
							</tr>
							<tr>
								<td><b>Description</b></td>
								<td>${subjectDto.description}</td>
							</tr>
							<tr>
								<td><b>Year of study</b></td>
								<td>${subjectDto.yearOfStudy}</td>
							</tr>
							<tr>
								<td><b>Semester</b></td>
								<td>${subjectDto.semester.semester}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>