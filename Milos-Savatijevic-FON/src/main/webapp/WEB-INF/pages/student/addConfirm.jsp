<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add student</title>
</head>
<body>
<div><p/></div>
	<div class="container fill">
		<div class="row">
			<div class="col-md-7 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Register new student</h2>
						<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>
						<form:form method="post"
							action="${contextPath}/student/addSave"
							modelAttribute="studentDto" class="form-horizontal">
							<fieldset>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="firstname">Firstname:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="firstname" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="lastname">Lastname:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="lastname" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="indexNumber">Index number:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="indexNumber" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="email">Email:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="email" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="address">Address:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="address" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="phone">Phone:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="phone" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label"
										path="currentYearOfStudy">Current year of study:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="currentYearOfStudy" disabled="true"/>
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="cityDto">City</form:label>
									<div class="col-sm-8">
										<form:select class="form-control" path="cityDto" disabled="true">
											<form:options items="${cities}" itemValue="pttNumber"
												itemLabel="name" />
										</form:select>
									</div>
								</div>
								<h3>Are you sure you want to save student?</h3>
								<div class="form-group">
									<div class="col-md-4">
										<button class="btn btn-primary" id="save" name="action"
											value="save">Save</button>
										<button class="btn btn-primary" id="cancel" name="action"
											value="cancel">Cancel</button>
									</div>
								</div>
							</fieldset>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>