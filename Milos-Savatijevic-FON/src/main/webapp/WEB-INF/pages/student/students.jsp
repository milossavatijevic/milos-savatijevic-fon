<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Students</title>
<style>
#container {
	height: 70vh;
}

.error {
	color: red;
}
</style>
</head>
<body>

	<div>
		<c:url value="/home" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark bg-light">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav mx-auto">
					<c:url value="/student/add" var="addStudent" />
					<li class="nav-item "><a class="nav-link text-primary"
						href="<c:out value="${addStudent}"/>">Add new student</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container fill" id="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Students</h2>
						<c:if test="${not empty errorMessage}">
							<div class="error">${errorMessage}</div>
						</c:if>
						<table class="table table-hover" id="students">
							<thead>
								<tr>
									<th scope="col">Index number</th>
									<th scope="col">Firstname</th>
									<th scope="col">Lastname</th>
									<td colspan="3"></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${students}" var="student">

									<c:url value="/student/details" var="detailsStudent">
										<c:param name="id" value="${student.id}" />
									</c:url>
									<c:url value="/student/edit" var="editStudent">
										<c:param name="id" value="${student.id}" />
									</c:url>
									<c:url value="/student/delete" var="deleteStudent">
										<c:param name="id" value="${student.id}" />
									</c:url>
									<tr>
										<td>${student.indexNumber}</td>
										<td>${student.firstname}</td>
										<td>${student.lastname}</td>
										<td><a href="<c:out value = "${detailsStudent}"/>">Details</a></td>
										<td><a href="<c:out value = "${editStudent}"/>">Edit</a></td>
										<td><a href="<c:out value = "${deleteStudent}"/>">Delete</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<nav>
							<div id="pagination" class="pagination justify-content-center"
								style="flex-wrap: wrap;">
								<div class="page-item page-link">

									<c:forEach begin="1" end="${maxPages}" varStatus="i">
										<c:url value="/student" var="url">
											<c:param name="page" value="${i.index}" />
										</c:url>
										<c:choose>
											<c:when test="${page == i.index}">
												<span>${i.index}</span>
											</c:when>
											<c:otherwise>
												<a href='<c:out value="${url}" />'>${i.index}</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>