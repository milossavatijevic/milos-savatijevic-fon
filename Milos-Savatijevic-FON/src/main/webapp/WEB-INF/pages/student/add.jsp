<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add student</title>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div>
		<c:url value="/student" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>


	<div class="container fill">
		<div class="row">
			<div class="col-md-7 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Register new student</h2>
						<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>
						<form:form method="post"
							action="${contextPath}/student/addConfirm"
							modelAttribute="studentDto" class="form-horizontal">
							<fieldset>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="firstname">Firstname:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="firstname" />
										<form:errors path="firstname" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="lastname">Lastname:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="lastname" />
										<form:errors path="lastname" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="indexNumber">Index number:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="indexNumber" />
										<form:errors path="indexNumber" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="email">Email:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="email" />
										<form:errors path="email" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="address">Address:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="address" />
										<form:errors path="address" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="phone">Phone:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="phone" />
										<form:errors path="phone" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label"
										path="currentYearOfStudy">Current year of study:</form:label>
									<div class="col-sm-8">
										<form:input class="form-control" path="currentYearOfStudy" />
										<form:errors path="currentYearOfStudy" cssClass="error" />
									</div>
								</div>

								<div class="form-group row">
									<form:label class="col-sm-3 col-form-label" path="cityDto">City</form:label>
									<div class="col-sm-8">
										<form:select class="form-control" path="cityDto">
											<form:options items="${cities}" itemValue="pttNumber"
												itemLabel="name" />
										</form:select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4">
										<button class="btn btn-primary" id="save" name="operation"
											value="save">Save</button>
									</div>
								</div>
							</fieldset>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>