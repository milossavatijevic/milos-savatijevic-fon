<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
	<div>
		<c:url value="/student" var="back" />
		<a href="<c:out value="${back}"/>">&larr;Back</a>
	</div>
	<div class="container fill">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">${studentDto.firstname}&ensp;${studentDto.lastname}</h2>
						<table class="table table-hover">
							<tr>
								<td><b>Index number</b></td>
								<td>${studentDto.indexNumber}</td>
							</tr>
							<tr>
								<td><b>Firstname</b></td>
								<td>${studentDto.firstname}</td>
							</tr>
							<tr>
								<td><b>Lastname</b></td>
								<td>${studentDto.lastname}</td>
							</tr>
							<tr>
								<td><b>Email</b></td>
								<td>${studentDto.email}</td>
							</tr>
							<tr>
								<td><b>Address</b></td>
								<td>${studentDto.address}</td>
							</tr>
							<tr>
								<td><b>Phone</b></td>
								<td>${studentDto.phone}</td>
							</tr>
							<tr>
								<td><b>Current year of study</b></td>
								<td>${studentDto.currentYearOfStudy}</td>
							</tr>
							<tr>
								<td><b>City</b></td>
								<td>${studentDto.cityDto.name}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

