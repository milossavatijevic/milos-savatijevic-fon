DROP TABLE IF EXISTS `application`;
DROP TABLE IF EXISTS `exam`;
DROP TABLE IF EXISTS `professor`;
DROP TABLE IF EXISTS `student`;
DROP TABLE IF EXISTS `subject`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `title`;
DROP TABLE IF EXISTS `city`;


CREATE TABLE `city` (
  `PTT_NUMBER` bigint NOT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`PTT_NUMBER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `title` (
  `TITLE_ID` bigint NOT NULL AUTO_INCREMENT,
  `NAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`TITLE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `user` (
  `USER_ID` bigint NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(255) NOT NULL,
  `LASTNAME` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `subject` (
  `SUBJECT_ID` bigint NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `NAME` varchar(30) NOT NULL,
  `SEMESTER` varchar(255) DEFAULT NULL,
  `YEAR_OF_STUDY` bigint DEFAULT NULL,
  PRIMARY KEY (`SUBJECT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `student` (
  `STUDENT_ID` bigint NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(50) DEFAULT NULL,
  `CURRENT_YEAR_OF_STUDY` bigint NOT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `FIRSTNAME` varchar(30) NOT NULL,
  `INDEX_NUMBER` varchar(10) NOT NULL,
  `LASTNAME` varchar(30) NOT NULL,
  `PHONE` varchar(15) DEFAULT NULL,
  `CITY_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`STUDENT_ID`),
  UNIQUE KEY `UK_rs1bpc3ugdu5oywseoxb8r3a3` (`INDEX_NUMBER`),
  UNIQUE KEY `UK_ry4x62xvsk185y64dbiemdrmn` (`EMAIL`),
  KEY `FKca8u7icqjw4wggr5bdd4xt4o4` (`CITY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `professor` (
  `PROFESSOR_ID` bigint NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(50) DEFAULT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `FIRSTNAME` varchar(30) NOT NULL,
  `LASTNAME` varchar(30) NOT NULL,
  `PHONE` varchar(15) DEFAULT NULL,
  `REELECTION_DATE` date NOT NULL,
  `CITY_ID` bigint DEFAULT NULL,
  `TITLE_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`PROFESSOR_ID`),
  UNIQUE KEY `UK_tba9qcekj0riiy55sgefti3f` (`EMAIL`),
  KEY `FKqn4b7cg6kal255ibr77al9cxs` (`CITY_ID`),
  KEY `FKhqopc76vdt0ssul52mqv8xxjk` (`TITLE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `exam` (
  `EXAM_ID` bigint NOT NULL AUTO_INCREMENT,
  `EXAM_DATE` date NOT NULL,
  `PROFESSOR_ID` bigint DEFAULT NULL,
  `SUBJECT_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`EXAM_ID`),
  KEY `FKaxutdl6uyj1bn64of0yxvftk6` (`PROFESSOR_ID`),
  KEY `FKtc7oncqbiyjnckglmvepqa239` (`SUBJECT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `application` (
  `APPLICATION_ID` bigint NOT NULL AUTO_INCREMENT,
  `EXAM_ID` bigint DEFAULT NULL,
  `STUDENT_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`APPLICATION_ID`),
  KEY `FK28q71wqb2lodo03escvrvpkw0` (`EXAM_ID`),
  KEY `FKgcjvostbyxdivopnitcsal7sk` (`STUDENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci